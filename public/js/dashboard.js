$(document).ready(function(){
  	/*===== Date Picker Start =====*/
	$("#datepicker").datepicker({
		dateFormat:"dd-mm-yy",
		changeMonth: true,
		changeYear: true,
		yearRange: "1945:2019"});
  /*===== Date Picker End =====*/
  
    $( "#add_user" ).click(function() {
        $('#form_add_user').show('slow');
      });

      $( ".exit" ).click(function() {
        $('#form_add_user').hide('slow');
      });


      function readURL(input,id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $(id).attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }

        console.log('bismillah')
    }

    $("#photo").change(function(){
        readURL(this,'#img-upload');
    }); 

    $("#mosque_photo").change(function(){
      readURL(this,'#mosque_img_upload');
  }); 
})