<?php

namespace App\Http\Controllers\FrontPage\Takmir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Blog;
use App\Models\Credit;
use App\Models\Debet;
use App\Models\Gallery;
use App\Models\Jamaah;
use App\Models\Takmir;
use App\User;
use App\Models\Organization;
use App\Models\JobdescOrg;
use App\Models\Mosque;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WebPageController extends Controller
{
    public function index($uuid, $name)
    {
            $user = User::where('name', $uuid)->first();
            $takmir = Takmir::where('user_id',$user->id)->first();
            $blog = Blog::where('user_id',$user->id)
                        ->where('kategori', 'jadwal pengajian')
                        ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                        ->get();
            
            $org = Organization::where('takmir_id',$takmir->id)->first();
            $job = JobdescOrg::where('takmir_id',$takmir->id)->first();
            
            $mosque = Mosque::where('slug', $name)
                            ->where('takmir_id',$takmir->id)
                            ->first();

            $album = Album::where('takmir_id', $takmir->id)
                            // ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                            // ->select('*', 'galleries.id as id_foto', 'galleries.slug as slug_foto')
                            ->get();
            foreach($album as $albums){
            $gallery =Gallery::where('album_id', $albums->id)->first();
            }
            
            
            $takmir1 = DB::table('takmirs')->where('user_id',$user->id)->first();
            
            $debit = Debet::where('takmir_id', $takmir1->id)
                            ->get()
                            ->sum('nominal');
                
                        
            $week = Debet::where('takmir_id', $takmir1->id)
                            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                            ->get()
                            ->sum('nominal');
                        
            $month = Debet::where('takmir_id', $takmir1->id)
                            ->whereMonth('created_at',Carbon::now()->format('m'))
                            ->get()
                            ->sum('nominal');
                        
                        
            $credit = Credit::where('takmir_id', $takmir1->id)
                            ->get()
                            ->sum('nominal');
                        
                        
            $weekcredit = Credit::where('takmir_id', $takmir1->id)
                            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                            ->get()
                            ->sum('nominal');
                        
            $monthcredit = Credit::where('takmir_id', $takmir1->id)
                            ->whereMonth('created_at',Carbon::now()->format('m'))
                            ->get()
                            ->sum('nominal');
            
            $jamaah = count(Jamaah::where('takmir_id', $takmir1->id)
                            ->get());

            $blog1 = count(Blog::where('user_id',$user->id)
                    ->where('kategori', 'jadwal pengajian')
                    ->get());
                            // dd($blog1);

            $route = 'web/'.$user->name.'/'. $mosque->slug ;
            $exp = explode('/', $route,0);
            // dd($exp);
            $imp = implode('/', $exp);

            $data = [
                'user' =>$user,
                'takmir'=> $takmir,
                'org' => $org,
                'job' => $job,
                'mosque' => $mosque,
                'debit' => $debit,
                'credit' => $credit,
                'week' => $week,
                'month' => $month,
                'weekcredit' => $weekcredit,
                'monthcredit' => $monthcredit,
                'jamaah' => $jamaah,
                'blog' => $blog,
                'blog1' => $blog1,
                'exp' => $exp,
                'album' => $album,
                'gallery' => $gallery
                // 'imp' => $imp

            ];
            
            return view('frontpage.takmir.index',compact('data'));
        
    }
}
