<?php

namespace App\Http\Controllers\FrontPage\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Blog;
use App\Models\Gallery;
use App\Models\Mosque;
use App\Models\Takmir;
use App\User;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    public function index(){
        // $blog = Blog::where('cat','admin')->get();
            
            $blog = Blog::where('kategori', 'blog')
                        // ->where('cat','admin')
                        ->get();
            
            return view('frontpage.main.blog.index',compact('blog'));
            
            
            
        }
        
    public function takmir($uuid, $name)
    {
        $user = User::where('name', $uuid)->first();
        $takmir = Takmir::where('user_id',$user->id)->first();
        $blog = Blog::where('cat','takmir')
                    ->where('user_id', $user->id)
                    ->where('kategori', 'blog')
                    ->get();
        $mosque = Mosque::where('slug', $name)
                        ->where('takmir_id',$takmir->id)
                        ->first();
                    // dd(Auth::user()->name);
        $route = 'web/'.$user->name.'/'. $mosque->slug ;
        $exp = explode('/', $route,0);
        // dd($exp);
        $imp = implode('/', $exp);


        return view('frontpage.takmir.blog.index',compact('blog', 'imp', 'exp', 'mosque'));        
    }

    public function takmirshow($slug){
        $blog = Blog::where('slug',$slug)->first();

        return view('frontpage.takmir.blog.detail',compact('blog'));
    }
    
    public function gallery($uuid)
    {
        $album = Album::where('uuid', $uuid)->first();
        $gallery = Gallery::where('album_id', $album->id)->get();
        
        $data = [
            'album' => $album,
            'gallery' => $gallery
        ];

        return view('frontpage.takmir.gallery.detail',compact('data'));
    }

    public function show($slug){
        $blog = Blog::where('slug',$slug)->first();

        return view('frontpage.main.blog.detail',compact('blog'));
    }
}
