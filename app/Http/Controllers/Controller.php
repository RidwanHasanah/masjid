<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Yajra\DataTables\DataTables;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function displayTakmir($user)
    {
        return DataTables::of($user)
        ->addColumn('action', function ($user) {
            $url = route('takmir.show',$user->uuid);
            return "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
            "<a style='margin-top:0.5em; width:10em;' href='".route('takmir.edit',$user->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
            "<a style='margin-top:0.5em; width:10em;' onclick='deleteUser(".$user->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
        })->make(true);
    }

    public function displayAdmin($user)
    {
        return DataTables::of($user)
        ->addColumn('action', function ($user) {
            $url = route('admin.show',$user->uuid);
            return "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
            "<a style='margin-top:0.5em; width:10em;' href='".route('admin.edit',$user->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
            "<a style='margin-top:0.5em; width:10em;' onclick='deleteUser(".$user->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
        })->make(true);
    }

    public function displayJamaah($jamaah)
    {
        return DataTables::of($jamaah)
        ->addColumn('action', function ($jamaah) {
            $url = route('jamaah.show',$jamaah->uuid);
            return "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
            "<a style='margin-top:0.5em; width:10em;' href='".route('jamaah.edit',$jamaah->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
            "<a style='margin-top:0.5em; width:10em;' onclick='deleteUser(".$jamaah->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
        })->make(true);
    }

    public function displayBlog($blog)
    {
        return DataTables::of($blog)
        ->addColumn('action', function ($blog) {
            $url = route('blog.show',$blog->uuid);
            return "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
            "<a style='margin-top:0.5em; width:10em;' href='".route('blog.edit',$blog->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
            "<a style='margin-top:0.5em; width:10em;' onclick='deletePost(".$blog->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
        })->editColumn('created_at',function($blog){
            $date = date('d - m - Y',strtotime($blog->created_at));
            return $date;
        })
        ->make(true);
    }
    
    public function displayDebit($debit)
    {
        return DataTables::of($debit)
                ->addColumn('action', function ($debit) {
                    $url = route('debit.show',$debit->uuid);
                    return 
                    "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
                    "<a style='margin-top:0.5em; width:10em;' href='".route('debit.edit',$debit->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
                    "<a style='margin-top:0.5em; width:10em;' onclick='deleteDebit(".$debit->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
                })
                ->editColumn('created_at',function($debit){
                    $date = tanggalindonesia(date('Y-m-d',strtotime($debit->created_at)));
                    return $date;
                })
                // ->addColumn('total', function($debit) {
                //     return $debit;
                // })
                ->make(true);        
            }
            
            public function displayCredit($credit)
            {
                return DataTables::of($credit)
                        ->addColumn('action', function ($credit) {
                            $url = route('credit.show',$credit->uuid);
                            return 
                            "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
                            "<a style='margin-top:0.5em; width:10em;' href='".route('credit.edit',$credit->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
                            "<a style='margin-top:0.5em; width:10em;' onclick='deleteCredit(".$credit->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
                        })
                        ->editColumn('created_at',function($credit){
                            $date = tanggalindonesia(date('Y-m-d',strtotime($credit->created_at)));
                            return $date;
                        })
                        ->make(true);        

            }
            
            public function displaySaran($saran)
            {
                return DataTables::of($saran)
                        ->addColumn('action', function ($saran) {
                            $url = route('saran.show',$saran->uuid);
                            return 
                            "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
                            // "<a style='margin-top:0.5em; width:10em;' href='".route('saran.edit',$saran->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
                            "<a style='margin-top:0.5em; width:10em;' onclick='deletesaran(".$saran->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
                        })
                        ->editColumn('created_at',function($saran){
                            $date = tanggalindonesia(date('Y-m-d',strtotime($saran->created_at)));
                            return $date;
                        })
                        ->make(true);        

            }

            public function displayfoto($album)
            {
                return DataTables::of($album)
                        ->addColumn('action', function ($album) {
                            $url = route('gallery.show',$album->uuid);
                            return 
                            "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Show</i></a>&nbsp;".
                            "<a style='margin-top:0.5em; width:10em;' href='".route('gallery.edit',$album->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
                            "<a style='margin-top:0.5em; width:10em;' onclick='deleteAlbum(".$album->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
                        })
                        ->editColumn('created_at',function($album){
                            $date = tanggalindonesia(date('Y-m-d',strtotime($album->created_at)));
                            return $date;
                        })
                        ->make(true);        

            }
}
