<?php

namespace App\Http\Controllers;

use App\Models\Credit;
use App\Models\Debet;
use App\Models\Jamaah;
use App\Models\Mosque;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->hasRole('master') || Auth::user()->hasRole('admin')) {
            $takmir = count(UserRole::where('role_id', 3)->get());
            $admin = count(UserRole::where('role_id', 2)->get());
            $masjid = count(Mosque::all());
            $Jamaah = count(Jamaah::all()); 

            return view('dashboard.components.home', compact(
                'takmir',
                'admin',
                'masjid',
                'Jamaah'
            ));           
        }

        elseif (!Auth::user()->hasRole('master') && !Auth::user()->hasRole('admin')) {
            $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
            
            $debit = Debet::where('takmir_id', $takmir1->id)
                            ->get()
                            ->sum('nominal');
                
                        
            $week = Debet::where('takmir_id', $takmir1->id)
                            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                            ->get()
                            ->sum('nominal');
                        
            $month = Debet::where('takmir_id', $takmir1->id)
                            ->whereMonth('created_at',Carbon::now()->format('m'))
                            ->get()
                            ->sum('nominal');
                        
                        
            $credit = Credit::where('takmir_id', $takmir1->id)
                            ->get()
                            ->sum('nominal');
                        
                        
            $weekcredit = Credit::where('takmir_id', $takmir1->id)
                            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                            ->get()
                            ->sum('nominal');
                        
            $monthcredit = Credit::where('takmir_id', $takmir1->id)
                            ->whereMonth('created_at',Carbon::now()->format('m'))
                            ->get()
                            ->sum('nominal');
            
            $jamaah = count(Jamaah::where('takmir_id', $takmir1->id)
                            ->get());
                          
                            
            return view('dashboard.components.home', compact(
                'debit',
                'credit',
                'week',
                'month',
                'weekcredit',
                'monthcredit',
                'jamaah'
            ));
        }
    }
}
