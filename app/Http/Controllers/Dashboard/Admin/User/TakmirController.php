<?php

namespace App\Http\Controllers\Dashboard\Admin\User;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Models\UserRole;
use App\Models\Takmir;
use App\Models\Admin;
use App\Models\Mosque;
use App\Models\Organization;
use App\Models\JobdescOrg;

class TakmirController extends Controller
{

    public function index()
    {
        return view('dashboard.admin.user.takmir.all_user');        
    }

    
    public function create()
    {
        return view('dashboard.admin.user.takmir.add_user');
    }
    
    public function store(Request $request)
    {
        $user = new User;
        $takmir = new Takmir;
        $mosque = new Mosque;
        $org = new Organization;
        $jobdesc = new JobdescOrg;

        $request->validate([
            'name' => 'required|max:255|min:3',
            'email' => 'required|unique:users',
            'password' => 'required|min:8'
        ]);
        
        $user->uuid = Uuid::uuid4();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        switch ($request->role) {
            case 3:
                $takmir->user_id = $user->id;
                $takmir->save();

                $mosque->takmir_id = $takmir->id;
                $mosque->save();

                $org->takmir_id = $takmir->id;
                $org->uuid = Uuid::uuid4();
                $org->save();
                
                $jobdesc->takmir_id = $takmir->id;
                $jobdesc->uuid = Uuid::uuid4();
                $jobdesc->save();
                // dd($mosque);

                UserRole::create(['user_id' => $user->id,'role_id' => 3]);
                break;
            case 2:
                Admin::create(['user_id'=>$user->id]);
                UserRole::create(['user_id' => $user->id,'role_id' => 3]);
                UserRole::create(['user_id' => $user->id,'role_id' => 2]);
                break;
        }

        return redirect()->route('takmir.index')->with('success','Berhasil Menambahkan'); 
    }

    
    public function show($id)
    {
        $user = User::where('uuid',$id)->first();
        $takmir = $user->takmir()->first();
        $mosque = $takmir->mosque()->first();

        // dd($mosque);

        return view('dashboard.admin.user.takmir.detail_user',compact('user','takmir','mosque'));
    }
    
    public function edit($id)
    {
        $user = User::where('uuid',$id)->first();
        $takmir = $user->takmir()->first();
        $mosque = $takmir->mosque()->first();
        // dd($takmir);

        return view('dashboard.admin.user.takmir.edit_user',compact('user','takmir','mosque'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'email' => 'required',
            'password' => 'required|min:8'
        ]);
        
        $user = User::where('uuid',$id)->first();
        // dd($user);
        $takmir = $user->takmir()->first();
        $mosque = $takmir->mosque()->first();

        //============ User =========
        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password != $user->password) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        //=========== Takmir =============
        $takmir->province = $request->province;
        $takmir->address = $request->address;
        $takmir->hp = $request->hp;
        $takmir->birth_date = date('Y-m-d', strtotime($request->birth_date));
        $takmir->birth_place = $request->birth_place;
        $takmir->gender = $request->gender;

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $user->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/user',$photoName);
            $takmir->photo = $photoName;
        }
        $takmir->save();

        // ============ Mosque ===========
        $mosque->name = $request->mname;
        $mosque->province = $request->mprovince;
        $mosque->address = $request->maddress;
        $mosque->desc = $request->mdesc;

        if ($request->hasFile('mphoto')) {
            $photo = $request->file('mphoto');
            $photoName = $user->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/mosque',$photoName);
            $mosque->photo = $photoName;
        }

        $mosque->save();


        return redirect()->back()->with('success','Edit Success');
    }
    
    public function destroy($id)
    {
        $takmir = Takmir::find($id);
        
        $user = User::where('id',$takmir->user_id)->first();
        $user->delete();
    }

    public function takmirApi(){
        $user = DB::table('users')->rightJoin('takmirs','takmirs.user_id','=','users.id');

        return $this->displayTakmir($user);
    }
}
