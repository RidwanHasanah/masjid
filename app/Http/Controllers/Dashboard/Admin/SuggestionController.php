<?php

namespace App\Http\Controllers\Dashboard\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Suggestion;
use App\Models\Takmir;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;



class SuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.components.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.takmir.suggestion.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();

        $suggestion = new Suggestion;
        $suggestion->uuid = Uuid::uuid4();
        $suggestion->takmir_id = $takmir->id;
        $suggestion->subject = $request->subject;
        $suggestion->suggest = $request->pesan;

        $suggestion->save();

        return redirect()->back()->with('success','Pesan Terkirim');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $saran = User::rightJoin('takmirs', 'takmirs.user_id', '=', 'users.id')
                    ->rightJoin('suggestions', 'suggestions.takmir_id', '=', 'takmirs.id')
                    ->rightJoin('mosques', 'mosques.takmir_id', '=', 'takmirs.id')
                    ->select('*', 'mosques.name as masjid', 'users.name as name','suggestions.uuid as suggest_uuid','suggestions.id as suggest_id', 'suggestions.created_at as suggest_at', 'mosques.photo as photo_masjid')
                    ->get();
                    // dd($saran);
        foreach ($saran as $suggest ) {
            $saran = Suggestion::where('uuid',$suggest->suggest_uuid,$id)->get();
        }
        // dd($suggest);

        return view('dashboard.takmir.suggestion.detail', compact('suggest', 'saran')); 
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $saran = Suggestion::where('id', $id);
        $saran->delete();
    }

    public function saranapi()
    {   
        // $takmir = User::rightJoin('takmirs', 'takmirs.user_id', '=', 'users.id')
        //             ->rightJoin('suggestions', 'suggestions.takmir_id', '=', 'takmirs.id')
        //             ->rightJoin('mosques', 'mosques.takmir_id', '=', 'takmirs.id')
        //             ->select('*', 'mosques.name as masjid', 'users.name as name', 'suggestions.created_at as suggest_at','suggestions.uuid as suggest_uuid','suggestions.id as suggest_id', 'mosques.photo as photo_masjid')
        //             ->get();
        // $saran = $takmir;
        
        $saran = Suggestion::get();
        // dd($saran);

        return $this->displaySaran($saran);
    }
}
