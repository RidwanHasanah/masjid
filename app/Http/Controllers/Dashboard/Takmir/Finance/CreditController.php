<?php

namespace App\Http\Controllers\Dashboard\Takmir\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Credit;
use App\Models\Debet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.takmir.finance.out.index');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.takmir.finance.out.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();

        $credit = new Credit;

        $credit->uuid = Uuid::uuid4();
        $credit->takmir_id = $takmir->id;
        $credit->info = $request->info;
        $credit->nominal = $request->nominal;

        $credit->save();

        
        return redirect()->route('credit.index')->with('success','Berhasil Menambahkan');
        
        // $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        
        //             ->sum('nominal');
        
        // $debit1 = new Debet;
        // $debit1->takmir_id = $takmir->id;
        // $debit1->nominal -= $request->nominal;
        
        // $debit1->update();
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();

        $credit = Credit::where('takmir_id', $takmir1->id)
                    ->where('uuid', $id)
                    ->first();
        
        return view('dashboard.takmir.finance.out.detail',compact('credit'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();

        $credit = Credit::where('takmir_id', $takmir1->id)
                    ->where('uuid', $id)
                    ->first();

        return view('dashboard.takmir.finance.out.update', compact('credit'));

    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();

        $credit = Credit::where('takmir_id', $takmir1->id)
                    ->where('uuid', $id)
                    ->first();

        $credit->info = $request->info;
        $credit->nominal = $request->nominal;

        $credit->save();

        return redirect()->back()->with('success','Edit Success');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();

        $credit = Credit::where('takmir_id', $takmir1->id)
                        ->first();

        $credit->delete();
    }
    
    public function apicredit()
    {
        $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
            
        $credit = Credit::where('takmir_id', $takmir1->id)
                    // ->rightJoin('credits', 'credits.debet_id', '=', 'debets.id')
                    ->get();
        
        
        return $this->displayCredit($credit);
    }
}
