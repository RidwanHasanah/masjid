<?php

namespace App\Http\Controllers\Dashboard\Takmir\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Debet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class DebetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.takmir.finance.in.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.takmir.finance.in.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        $debit = new Debet;

        $debit->uuid = Uuid::uuid4();
        $debit->takmir_id = $takmir->id;
        $debit->info = $request->info;
        $debit->nominal = $request->nominal;

        $debit->save();

        return redirect()->route('debit.index')->with('success','Berhasil Menambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();

        $debit = Debet::where('takmir_id', $takmir1->id)
                    ->where('uuid', $id)
                    ->first();
                    
        return view('dashboard.takmir.finance.in.detail', compact('debit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();

        $debit = Debet::where('takmir_id', $takmir1->id)
                    ->where('uuid', $id)
                    ->first();
                    
        return view('dashboard.takmir.finance.in.update', compact('debit'));

    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $takmir1 = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();

        $debit = Debet::where('takmir_id', $takmir1->id)
                    ->where('uuid', $id)
                    ->first();

        $debit->info = $request->info;
        $debit->nominal = $request->nominal;

        $debit->save();

        return redirect()->back()->with('success','Edit Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        
        $debit = Debet::where('takmir_id', $takmir->id)->first();

        $debit->delete();
        
    }

    public function apidebit()
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        
        $debit = Debet::where('takmir_id', $takmir->id)
                    ->get();
                    

        return $this->displayDebit($debit);
    }
}
