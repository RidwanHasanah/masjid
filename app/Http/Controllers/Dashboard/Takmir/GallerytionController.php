<?php

namespace App\Http\Controllers\Dashboard\Takmir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Gallery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class GallerytionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.takmir.gallery.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.takmir.gallery.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        $gallery = new Gallery;
        $album = new Album;
        $album->uuid = Uuid::uuid4();
        $album->takmir_id = $takmir->id;
        $album->name = $request->album;
        
        $exp = explode(' ',$request->album);
        $imp = implode('-',$exp);
        $slug = Album::where('slug',$imp)->first();

        if ( strlen($slug == 0)) {
            $album->slug = $imp;
        }elseif ( strlen($slug != 0)){
            $album->slug = $request->album;
        }else{
            $album->slug = Uuid::uuid4();
        }

        $album->save();


        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $gallery->takmir_id = $takmir->id;
        $gallery->album_id = $album->id;
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $photo->getClientOriginalName();
            $photo->storeAs('public/gallery',$photoName);
            $gallery->photo = $photoName;


        }
        $exp = explode(' ',$photoName);
        $imp = implode('-',$exp);
        $slug = Gallery::where('slug',$imp)->first();

        if ( strlen($slug == 0)) {
            $gallery->slug = $imp;
        }elseif ( strlen($slug != 0)){
            $gallery->slug = $$photoName;
        }else{
            $gallery->slug = Uuid::uuid4();
        }

        $gallery->save();

        return redirect()->route('gallery.index')->with('success','Berhasil Menambahkan Photo');

    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('uuid', $id)
                        // ->where('takmir_id', $takmir->id)
                        ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        ->select('*', 'galleries.id as id_foto', 'galleries.slug as slug_foto')
                        ->get();

        return view('dashboard.takmir.gallery.detail', compact('takmir', 'album'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('uuid', $id)
                        // ->where('takmir_id', $takmir->id)
                        // ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        // ->select('*', 'galleries.id as id_foto')
                        ->first();
        $data=[
            'album' => $album
        ];

        return view('dashboard.takmir.gallery.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        $album = Album::where('uuid', $id)->first();
        $album->name = $request->album;
        
        $exp = explode(' ',$request->album);
        $imp = implode('-',$exp);
        $slug = Album::where('slug',$imp)->first();

        if ( strlen($slug == 0)) {
            $album->slug = $imp;
        }elseif ( strlen($slug != 0)){
            $album->slug = $request->album;
        }else{
            $album->slug = Uuid::uuid4();
        }

        $album->update();
        return redirect()->back()->with('success', 'berhasil merubah album');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $album = Album::where('id', $id)->first();
        $album->delete();

        $album1 = Gallery::where('album_id', $album->id)->first();
        $album1->delete();
    }

    public function apifoto()
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        
        $album = Album::where('takmir_id', $takmir->id)->get();
        
        return $this->displayfoto($album);

    }


    // FOTO
    public function createfoto($id)
    {
        $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('uuid', $id)
                        // ->where('takmir_id', $takmir->id)
                        // ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        // ->select('*', 'galleries.id as id_foto')
                        ->first();
        return view('dashboard.takmir.gallery.add_foto', compact('album'));
    }

    public function storefoto(Request $request,$id)
    {
        $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('uuid', $id)
                        // ->where('takmir_id', $takmir->id)
                        // ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        // ->select('*', 'galleries.id as id_foto')
                        ->first();
            
        $gallery = new Gallery;
        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $gallery->takmir_id = $takmir->id;
        $gallery->album_id = $album->id;
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName =$photo->getClientOriginalName();
            $photo->storeAs('public/gallery',$photoName);
            $gallery->photo = $photoName;

        }
        $exp = explode(' ',$photoName);
        $imp = implode('-',$exp);
        $slug = Gallery::where('slug',$imp)->first();

        if ( strlen($slug == 0)) {
            $gallery->slug = $imp;
        }elseif ( strlen($slug != 0)){
            $gallery->slug = $$photoName;
        }else{
            $gallery->slug = Uuid::uuid4();
        }
        

        $gallery->save();

                            
        return redirect()->route('gallery.show', $album->uuid)->with('success','Berhasil Menambahkan Photo ke Dalam Album');
    }

    public function editfoto($id)
    {
        $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('takmir_id', $takmir->id)
                        // ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        // ->select('*', 'galleries.id as id_foto')
                        ->first();
        
        $gallery = Gallery::where('slug', $id)
                            ->where('album_id', $album->id)
                            ->first();

        $data = [
            'album' => $album,
            'gallery' => $gallery
        ];

        return view('dashboard.takmir.gallery.edit_foto', compact('data'));

    }

    public function updatefoto(Request $request, $id)
    {
        $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('takmir_id', $takmir->id)
                        // ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        // ->select('*', 'galleries.id as id_foto')
                        ->first();
        
        $gallery = Gallery::where('slug', $id)
                            ->where('album_id', $album->id)
                            ->first();
        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        // $gallery->takmir_id = $takmir->id;
        // $gallery->album_id = $album->id;
        
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName =$photo->getClientOriginalName();
            $photo->storeAs('public/gallery',$photoName);
            $gallery->photo = $photoName;

        }
        $exp = explode(' ',$photoName);
        $imp = implode('-',$exp);
        $slug = Gallery::where('slug',$imp)->first();

        if ( strlen($slug == 0)) {
            $gallery->slug = $imp;
        }elseif ( strlen($slug != 0)){
            $gallery->slug = $$photoName;
        }else{
            $gallery->slug = Uuid::uuid4();
        }
        

        $gallery->save();

        return redirect()->route('gallery.show', $album->uuid)->with('success','Berhasil Mengubah Photo');
    }
}
