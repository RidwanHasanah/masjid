<?php

namespace App\Http\Controllers\Dashboard\Takmir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Organization;
use App\Models\JobdescOrg;
use App\Models\Mosque;
use App\Models\Takmir;
use App\User;
use Illuminate\Support\Facades\Auth;

class OrganizationController extends Controller
{
    public function index()
    {
        if (!Auth::user()->hasRole('master') && !Auth::user()->hasRole('admin')) {
            $user = User::where('id',Auth::user()->id)->first();
            $takmir = Takmir::where('user_id',$user->id)->first();
            $org = Organization::where('takmir_id',$takmir->id)->first();
            $job = JobdescOrg::where('takmir_id',$takmir->id)->first();
            $mosque = Mosque::where('takmir_id',$takmir->id)->first();
            $data = [
                'user' =>$user,
                'takmir'=> $takmir,
                'org' => $org,
                'job' => $job,
                'mosque' => $mosque
            ];
            
            // return view('frontpage.takmir.index',compact('data'));
            return view('dashboard.takmir.organization.organization',compact('org', 'data'));
        }

        // dd($job);

        // return view('dashboard.components.menu',compact('data'));
        // $org = Organization::where('uuid',$uuid)->first();

    }
    public function editStructure($uuid)
    {
        $org = Organization::where('uuid',$uuid)->first();
        return view('dashboard.takmir.organization.structure',compact('org'));
    }

    public function updateStructure(Request $request, $uuid)
    {
        Organization::where('uuid', $uuid)->update([
            'pelindung' => $request['pelindung'],
            'penasehat' => $request['penasehat'],
            'ketua' => $request['ketua'],
            'sekretaris' => $request['sekretaris'],
            'bendahara' => $request['bendahara'],
            'humas' => $request['humas'],
            'keagamaan' => $request['keagamaan'],
            'kepemudaan' => $request['kepemudaan'],
            'pelaksana_umum' => $request['pelaksana_umum'],
            'tarbiyah' => $request['tarbiyah'],
            'pengajian' => $request['pengajian'],
            'imam' => $request['imam'],
            'muadzin' => $request['muadzin'],
            'remaja' => $request['remaja'],
            'keamanan' => $request['keamanan'],
            'marbot' => $request['marbot'],
        ]);

        return redirect()->route('org',$uuid)->with('success','Berhasil Merubah');

    }

    public function editJobdesc($uuid)
    {
        $job = JobdescOrg::where('uuid',$uuid)->first();
        return view('dashboard.takmir.organization.jobdesc',compact('job'));
    }

    public function updateJobdesc(Request $request, $uuid)
    {
        JobdescOrg::where('uuid', $uuid)->update([
            'pelindung' => $request['pelindung'],
            'penasehat' => $request['penasehat'],
            'ketua' => $request['ketua'],
            'sekretaris' => $request['sekretaris'],
            'bendahara' => $request['bendahara'],
            'humas' => $request['humas'],
            'keagamaan' => $request['keagamaan'],
            'kepemudaan' => $request['kepemudaan'],
            'pelaksana_umum' => $request['pelaksana_umum'],
            'tarbiyah' => $request['tarbiyah'],
            'pengajian' => $request['pengajian'],
            'imam' => $request['imam'],
            'muadzin' => $request['muadzin'],
            'remaja' => $request['remaja'],
            'keamanan' => $request['keamanan'],
            'marbot' => $request['marbot'],
        ]);

        return redirect()->back()->with('success','Berhasil Merubah');

    }
}
