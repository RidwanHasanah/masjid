<?php

namespace App\Http\Controllers\Dashboard\Takmir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Takmir;
use App\User;
use App\Models\Organization;
use App\Models\JobdescOrg;
use App\Models\Mosque;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class ProfileController extends Controller
{
    public function index()
    {
        if (!Auth::user()->hasRole('master') && !Auth::user()->hasRole('admin')) {
            $user = User::where('id',Auth::user()->id)->first();
            $takmir = Takmir::where('user_id',$user->id)->first();
            $org = Organization::where('takmir_id',$takmir->id)->first();
            $job = JobdescOrg::where('takmir_id',$takmir->id)->first();
            $mosque = Mosque::where('takmir_id',$takmir->id)->first();
            $data = [
                'user' =>$user,
                'takmir'=> $takmir,
                'org' => $org,
                'job' => $job,
                'mosque' => $mosque
            ];
            
            // return view('frontpage.takmir.index',compact('data'));
            return view('dashboard.takmir.profile.profile',compact('data'));
            return view('dashboard.components.menu',compact('data'));
            return view('dashboard.master',compact('data'));
        }

        // dd($job);

    }

    public function edit($uuid)
    {
        $user = User::where('uuid',$uuid)->first();
        $takmir = Takmir::where('user_id',$user->id)->first();
        $mosque = Mosque::where('takmir_id',$takmir->id)->first();
        $data = [
            'user' => $user,
            'takmir'=>$takmir,
            'mosque'=>$mosque
        ];
        // dd($user);

        return view('dashboard.takmir.profile.edit_profile',compact('data'));
        
    }

    public function update(Request $request,$id)
    {
        $user = User::where('uuid',$id)->first();
        $takmir = Takmir::where('user_id',$user->id)->first();
        $mosque = Mosque::where('takmir_id',$takmir->id)->first();

        $user->name = $request->name;
        $takmir->address = $request->address;
        $takmir->province = $request->province;
        $takmir->hp = $request->hp;
        $takmir->birth_date = date('Y-m-d',strtotime($request->birth_date));
        $takmir->birth_place = $request->birth_place;
        $takmir->gender = $request->gender;
        $mosque->name = $request->mosque_name;
        $mosque->username = $request->mosque_username;
        $mosque->address = $request->mosque_address;
        $mosque->gmaps = $request->mosque_maps;
        $mosque->visi = $request->mosque_visi;
        $mosque->misi = $request->mosque_misi;
        $mosque->province = $request->mosque_province;
        $mosque->desc = $request->mosque_desc;

        $exp = explode(' ',$request->mosque_username);
        $imp = implode('_',$exp);
        $slug = Mosque::where('slug',$imp)->first();

        if ( strlen($slug == 0)) {
            $mosque->slug = $imp;
        }elseif ( strlen($slug != 0)){
            $mosque->slug = $request->mosque_username . Uuid::uuid1();
        }else{
            $mosque->slug = Uuid::uuid4();
        }

        if($request->hasFile('photo')){
            if (strlen($takmir->photo) != 0) {
                Storage::delete('public/takmir/' . $takmir->photo);
            }

            $photo = $request->file('photo');
            $photoName = $id.$photo->getClientOriginalName();
            $photo->storeAs('public/takmir',$photoName);
            $takmir->photo = $photoName;
        }

        if($request->hasFile('mosque_photo')){
            if (strlen($mosque->photo) != 0) {
                Storage::delete('public/mosque/' . $mosque->photo);
            }

            $mphoto = $request->file('mosque_photo');
            $mphotoName = $id.$mphoto->getClientOriginalName();
            $mphoto->storeAs('public/mosque',$mphotoName);
            $mosque->photo = $mphotoName;
        }

        $user->save();
        $takmir->save();
        $mosque->save();

        return redirect()->back()->with('success','Berhasil Menyimpan Perubahan');

        
    }

    public function passEdit($uuid)
    {
        $user = User::where('uuid',$uuid)->first();

        return view('dashboard.takmir.profile.edit_password',compact('user'));

    }

    public function passUpdate(Request $request, $uuid)
    {
        $user = User::where('uuid',$uuid)->first();
        $request->validate(['pass'=>'min:8']);

        if ($request->pass == $request->cpass) {
            $user->password = bcrypt($request->pass);
            $user->save();
            return redirect()->back()->with('success','Berhasil Menyimpan Perubahan');
        }else{
            return redirect()->back()->with('info','Password Tidak Sama');
        }

    }
}
