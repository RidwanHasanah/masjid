<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Gallery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class AlbumController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.takmir.gallery.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.takmir.gallery.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        $gallery = new Gallery;
        $album = new Album;
        $album->uuid = Uuid::uuid4();
        $album->takmir_id = $takmir->id;
        $album->name = $request->album;
        
        $exp = explode(' ',$request->album);
        $imp = implode('-',$exp);
        $slug = Album::where('slug',$imp)->first();

        if ( strlen($slug == 0)) {
            $album->slug = $imp;
        }elseif ( strlen($slug != 0)){
            $album->slug = $request->album;
        }else{
            $album->slug = Uuid::uuid4();
        }

        $album->save();


        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $gallery->takmir_id = $takmir->id;
        $gallery->album_id = $album->id;
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $album->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/gallery',$photoName);
            $gallery->photo = $photoName;
        }

        $gallery->save();

        return redirect()->route('gallery.index')->with('success','Berhasil Menambahkan Photo');

    }

    public function createfoto($id)
    {
        $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('uuid', $id)
                        // ->where('takmir_id', $takmir->id)
                        // ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        // ->select('*', 'galleries.id as id_foto')
                        ->first();
        return view('dashboard.takmir.gallery.add_foto', compact('album'));
    }

    public function storefoto(Request $request,$id)
    {
        $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('uuid', $id)
                        // ->where('takmir_id', $takmir->id)
                        // ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        // ->select('*', 'galleries.id as id_foto')
                        ->first();
            
        $gallery = new Gallery;
        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $gallery->takmir_id = $takmir->id;
        $gallery->album_id = $album->id;
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $album->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/gallery',$photoName);
            $gallery->photo = $photoName;
        }
        

        $gallery->save();

        return redirect()->route('gallery.show', $album->uuid)->with('success','Berhasil Menambahkan Photo ke Dalam Album');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('uuid', $id)
                        // ->where('takmir_id', $takmir->id)
                        ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        ->select('*', 'galleries.id as id_foto')
                        ->get();

        return view('dashboard.takmir.gallery.detail', compact('takmir', 'album'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $takmir = DB::table('takmirs')->where('user_id', Auth::user()->id)->first();
        $album = Album::where('uuid', $id)
                        // ->where('takmir_id', $takmir->id)
                        ->rightJoin('galleries', 'galleries.album_id', '=', 'albums.id')
                        ->select('*', 'galleries.id as id_foto')
                        ->get();

        return view('dashboard.takmir.gallery.detail', compact('takmir', 'album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function apifoto()
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        
        $album = Album::where('takmir_id', $takmir->id)->get();
        
        return $this->displayfoto($album);

    }
}
