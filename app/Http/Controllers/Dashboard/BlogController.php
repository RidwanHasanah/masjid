<?php

namespace App\Http\Controllers\Dashboard;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Blog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    
    public function index()
    {
        return view('dashboard.blog.all_post');
    }
    
    public function create()
    {
        return view('dashboard.blog.add_post');
    }
    
    public function store(Request $request)
    {
        $blog = new Blog;
        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg',
        ]);

        $blog->uuid = Uuid::uuid4();
        $blog->kategori = $request->kategori;
        $blog->title = $request->title;
        $blog->content = $request->content;
        $blog->user_id = Auth::user()->id;

        if (Auth::user()->hasRole('admin')) {
            $blog->cat = 'admin';
        }else{
            $blog->cat = 'takmir';
        }

        // Bikin Slug
        $exp = explode(' ',$request->title.mt_rand(1,1000));
        $imp = implode('_',$exp);
        $slug = Blog::where('slug',$imp)->first();

        if ( strlen($slug == 0)) {
            $blog->slug = $imp;
        }elseif ( strlen($slug != 0)){
            $blog->slug = $request->title . Uuid::uuid4();
        }else{
            $blog->slug = Uuid::uuid4();
        }

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $blog->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/blog',$photoName);
            $blog->photo = $photoName;
        }

        $blog->save();

        return redirect()->route('blog.index')->with('success','Berhasil Menambahkan Tulisan'); 

    }
    
    public function show($id)
    {
        $blog = Blog::where('uuid',$id)->first();

        return view('dashboard.blog.detail_post',compact('blog'));
    }
    
    public function edit($id)
    {
        $blog = Blog::where('uuid',$id)->first();
        // dd($blog);
        return view('dashboard.blog.edit_post',compact('blog'));
    }
    
    public function update(Request $request, $id)
    {
        $blog = Blog::where('uuid',$id)->first();
        
        $blog->kategori = $request->kategori;
        $blog->title = $request->title;
        $blog->content = $request->content;
        
        if ($request->hasFile('photo')) {
            if (strlen($blog->photo) != 0) {
                Storage::delete('public/blog/' . $blog->photo);
            }
            $photo = $request->file('photo');
            $photoName = $blog->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/blog',$photoName);
            $blog->photo = $photoName;
        }

        $blog->update();

        return redirect()->back()->with('success','Berhasil mengubah');
    }
    
    public function destroy($id)
    {
        $blog = Blog::where('id',$id)->first();

        // dd($blog);

        if (strlen($blog->photo) != 0) {
            Storage::delete('public/blog/' . $blog->photo);
        }

        $blog->delete();
        
    }

    public function blogApi()
    {
        $blog = Blog::where('user_id', Auth::user()->id)->get();

        return $this->displayBlog($blog);
    }
}
