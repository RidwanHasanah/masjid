<?php

namespace App\Http\Controllers\Auth;

use Ramsey\Uuid\Uuid;
use App\User;
use App\Models\Takmir;
use App\Models\Mosque;
use App\Models\UserRole;
use App\Http\Controllers\Controller;
use App\Models\JobdescOrg;
use App\Models\Organization;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User;
        $takmir = new Takmir;
        $user->name = $data['name'];
        $user->uuid = Uuid::uuid4();
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->save();

        $takmir->user_id = $user->id;
        $takmir->save();

        UserRole::create(['user_id' => $user->id, 'role_id' => 3]);

        $masjid = new Mosque;
        $masjid->takmir_id = $takmir->id;
        // $masjid->username = $user->name.mt_rand(1,1000);
        $exp = explode(' ',$user->name);
        $imp = implode('_',$exp);
        $slug = Mosque::where('slug',$imp)->first();

        if ( strlen($slug == 0)) {
            $masjid->slug = $imp;
        }elseif ( strlen($slug != 0)){
            $masjid->slug = $user->name . Uuid::uuid1();
        }else{
            $masjid->slug = Uuid::uuid4();
        }

        $masjid->save();

        
        Organization::create([
            'takmir_id' => $takmir->id,
            'uuid' => Uuid::uuid4()
        ]);

        JobdescOrg::create([
            'takmir_id' => $takmir->id,
            'uuid' => Uuid::uuid4()
        ]);

         //redirect()->back()->with('success','Terima kasih sudah mendaftar, Silahkan buka email Anda untuk aktivasi akun');
         return $user;
    }
}
