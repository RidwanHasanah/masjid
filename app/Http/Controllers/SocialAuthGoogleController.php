<?php

namespace App\Http\Controllers;

use App\Models\JobdescOrg;
use App\Models\Mosque;
use App\Models\Organization;
use App\Models\Takmir;
use App\Models\UserRole;
use Illuminate\Http\Request;
use App\User;
use Socialite;
use Auth;
use Exception;
use Ramsey\Uuid\Uuid;

class SocialAuthGoogleController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }


    public function callback()
    {
        try {        
            $googleUser = Socialite::driver('google')->stateless()->user();
            $existUser = User::where('email',$googleUser->email)->first();
            
            
            if($existUser) {
                Auth::loginUsingId($existUser->id, true);
            }
            else {
                $takmir = new Takmir;
                $user = new User;
                $user->uuid = Uuid::uuid4();
                $user->name = $googleUser->name;
                $user->email = $googleUser->email;
                $user->google_id = $googleUser->id;
                $user->password = bcrypt(rand(1,10000));
                $user->save();
                Auth::loginUsingId($user->id, true);

                
                $takmir->user_id = $user->id;
                $takmir->save();

                UserRole::create(['user_id' => $user->id, 'role_id' => 3]);

                $masjid = new Mosque;
                $masjid->takmir_id = $takmir->id;
                // $masjid->username = $user->name.mt_rand(1,1000);
                $exp = explode(' ',$user->name);
                $imp = implode('_',$exp);
                $slug = Mosque::where('slug',$imp)->first();

                if ( strlen($slug == 0)) {
                    $masjid->slug = $imp;
                }elseif ( strlen($slug != 0)){
                    $masjid->slug = $user->name . Uuid::uuid1();
                }else{
                    $masjid->slug = Uuid::uuid4();
                }

                $masjid->save();

                Organization::create([
                    'takmir_id' => $takmir->id,
                    'uuid' => Uuid::uuid4()
                ]);
        
                JobdescOrg::create([
                    'takmir_id' => $takmir->id,
                    'uuid' => Uuid::uuid4()
                ]);
            }
            return redirect()->to('/home');
        } 
        catch (Exception $e) {
            // dd($e);
            return $e;
        }
    }
}