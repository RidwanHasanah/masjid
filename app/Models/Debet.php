<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Debet extends Model
{
    protected $guarded = ['id'];
    protected $table = 'debets';

    public function takmir(){
        return $this->belongsTo('App\Models\Takmir');
    }
}