<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded = ['id'];
    protected $table = 'galleries';

    public function takmir()
    {
        return $this->belongsTo('App\Models\Takmir');
    }
}
