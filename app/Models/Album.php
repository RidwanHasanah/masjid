<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $guarded =['i'];
    protected $table = 'albums';

    public function gallery()
    {
        return $this->hasOne('App\Models\Gallery');
    }

    public function takmir()
    {
        return $this->belongsTo('App\Models\Takmir', 'takmir_id');
    }
}
