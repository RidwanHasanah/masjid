<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jamaah extends Model
{
    protected $guarded = ['id'];
    protected $table = 'jamaahs';

    public function masjid(){
        return $this->belongsTo('App\Models\Takmir');
    }
}
