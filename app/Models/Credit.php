<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $guarded = ['id'];
    protected $table = 'credits';

    public function takmir(){
        return $this->belongsTo('App\Models\Takmir');
    }
}
