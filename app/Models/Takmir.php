<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Takmir extends Model
{
    protected $guarded = ['id'];
    protected $table = 'takmirs';

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function mosque(){
        return $this->hasOne('App\Models\Mosque','takmir_id');
    }

    public function jamaah(){
        return $this->hasMany('App\Models\Jamaah','takmir_id');
    }

    public function suggest(){
        return $this->hasOne('App\Models\Suggestion','takmir_id');
    }

    public function oragnization(){
        return $this->hasOne('App\Models\Organization','takmir_id');
    }

    public function jobdesc(){
        return $this->hasOne('App\Models\JobdescOrg','takmir_id');
    }

    public function gallery(){
        return $this->hasOne('App\Models\Gallery', 'takmir_id');
    }
}
