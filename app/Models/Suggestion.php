<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $guarded = ['id'];
    protected $table = 'suggestions';

    public function takmir(){
        return $this->belongsTo('App\Models\Takmir');
    }
}
