@extends('front_page')

@section('content')
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one bg-login">
                <div class="row w-100">
                        <div class="col-lg-6 mx-auto">
                          <div class="auto-form-wrapper">
                            <div class="text-center">
                                <h3 class="">Verifikasi Email Anda</h3>
                                <hr>
                                <div class="card-body ">
                                    @if (session('resent'))
                                        <div class="alert alert-success" role="alert">
                                            {{ __('Kami telah mengirim link Verifikasi baru ke Email Anda.') }}
                                        </div>
                                    @endif
                                    Assalamu Alaikum , {{ ucwords( Auth::user()->name )}}
                                    Terima Kasih Sudah Mendaftar, <br>
                                    Sebelum melanjutkan login, Silahkan periksa email Anda untuk mem-Verifikasi email Anda.<br>
                                    Jika belum mendapatkan email Silahkan, 
                                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                        @csrf
                                        <button type="submit" class="btn btn-block btn-info">{{ __('Klik Tombol Ini') }}</button>.
                                        Untuk mengirim email baru.
                                    </form>
                                </div>
                                <a class="btn btn-info" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
