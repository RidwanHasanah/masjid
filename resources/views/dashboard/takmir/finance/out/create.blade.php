@extends('dashboard.master')
@section('title')
Tambah Pengeluaran
@endsection
@section('content')
<div class="row">
    <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Tambah Pengeluaran</h4>
              <form class="forms-sample" method="POST" action="{{ route('credit.store') }}" enctype="multipart/form-data">
                  {{csrf_field()}} {{ method_field('POST')}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Info</label>
                    <input required type="text" class="form-control" name="info" id="info" placeholder="Masukan Info">
                </div>

                <div class="form-group">
                  <label for="nominal">Nominal</label>
                  <input required type="number" class="form-control" name="nominal" id="nominal" placeholder="Masukan Nominal">
                </div>
                
                <button type="submit" class="btn btn-success mr-2">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection

