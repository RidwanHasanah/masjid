@extends('dashboard.master')
@section('title')
Info Pengeluaran
@endsection
@section('content')
<div class="row">
  @include('layouts.patrials.alerts')
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="card-title">
          <h5 class="">Info Pengeluaran</h5>
        </div>
        <div class="">
          <div class="row">
            
            <div class="table-responsive col-md-9 col-lg-9 ">
              <table class="table table-striped table-hover">
                <tbody>
                  <tr>
                    <td><b>Info </b></td>
                    <td>{{$credit->info}}</td>
                  </tr>
                  <tr>
                    <td><b>Nominal</b></td>
                    <td>Rp. {{number_format($credit->nominal, 0, '', '.')}}</td>
                  </tr>
                  <tr>
                    <td><b>Tanggal</b></td>
                    <!-- <td>{{$credit->created_at}}</td> -->
                    <td>{{tanggalindonesia(date('Y-m-d',strtotime($credit->created_at)))}}</td>
                  </tr>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection