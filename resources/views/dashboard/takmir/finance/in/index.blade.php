@extends('dashboard.master')
@section('title')
Kas Masuk
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                @include('layouts.patrials.alerts')
                <div class="row">
                    <div class="col-md-6"><h4 class="card-title"> Kas Masuk </h4></div>
                    <div class="col-md-6"><a class="btn btn-success float-right" href="{{route('debit.create')}}">Tambah Kas</a></div>
                </div>
                <div class="center-pos" id="loadingDiv">
                        <img class="loading" src="{{asset('img/loading.gif')}}">
                </div>
                <div class="table-responsive">
                    <table id="table-debit" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Info</th>
                                <th>Nominal</th>
                                <th>Tanggal Pengeluaran</th>
                                <th></th>
                            </tr>
                            
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    
    var ajaxApi = '{{route("api.debit")}}'
    var table = $('#table-debit').DataTable({
        // order: [[1,'desc']],
        processing: true,
        serverSide: true,
        ajax : ajaxApi,
        columns : [
            {data: 'id', name: 'id'},
            {data: 'info', name: 'info'},
            {data: 'nominal', name: 'nominal', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp' )},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action',ordertable: false, searchable:false},
        ]

    })
    
    // Delete Data
    function deleteDebit(id){
        // alert('bismillah');
        
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        Swal.fire({
          title: 'Kamu yakin ?',
          text: "Data yang di hapus tidak akan kembali!",
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Ya, Hapus ini!'
        }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "{{ url('debit') }}" + '/' + id,
            type: "DELETE",
            data: {'_method' : 'DELETE', '_token' : csrf_token},
            success: function(data){
              table.ajax.reload();
              swal({
                title:'Berhasil',
                text: 'Data Sudah di hapus',
                type: 'success',
                timer: '1500'
              })
            },
            error: function(){
              swal({
                title: 'Oops',
                text: 'Something went wrong',
                type: 'error',
                timer: '1500'
              })
            }
          })
        }
      })
    }
</script>
@endsection