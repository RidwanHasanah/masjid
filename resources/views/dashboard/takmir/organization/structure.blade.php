@extends('dashboard.master')
@section('title')
Struktur
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h3 class="">Struktur Organisasi Masjid</h3>
                <small class="text-info">Gunakan koma ( , ) jika nama lebih dari 1</small>
                <form class="form-sample" method="POST" action="{{route('org.update',$org->uuid)}}">
                    {{csrf_field()}} {{ method_field('PATCH')}}
                    <div class="row justify-content-center ">
                        <div class="col-md-6 form-group">
                            <label>Pelindung</label>
                            <input class="form-control" name="pelindung" id="pelindung" value="{{$org->pelindung}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Penasehat</label>
                            <input class="form-control" name="penasehat" id="penasehat" value="{{$org->penasehat}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Ketua</label>
                            <input class="form-control" name="ketua" id="ketua" value="{{$org->ketua}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Sekretaris</label>
                            <input class="form-control" name="sekretaris" id="sekretaris" value="{{$org->sekretaris}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Bendahara</label>
                            <input class="form-control" name="bendahara" id="bendahara" value="{{$org->bendahara}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Humas</label>
                            <input class="form-control" name="humas" id="humas" value="{{$org->humas}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Keagamaan</label>
                            <input class="form-control" name="keagamaan" id="keagamaan" value="{{$org->keagamaan}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Kepemudaan</label>
                            <input class="form-control" name="kepemudaan" id="kepemudaan" value="{{$org->kepemudaan}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Pelaksana Umum</label>
                            <input class="form-control" name="pelaksana_umum" id="pelaksana_umum" value="{{$org->pelaksana_umum}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Tarbiyah</label>
                            <input class="form-control" name="tarbiyah" id="tarbiyah" value="{{$org->tarbiyah}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Pengajian</label>
                            <input class="form-control" name="pengajian" id="pengajian" value="{{$org->pengajian}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Imam</label>
                            <input class="form-control" name="imam" id="imam" value="{{$org->imam}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Muadzin</label>
                            <input class="form-control" name="muadzin" id="muadzin" value="{{$org->muadzin}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Remaja</label>
                            <input class="form-control" name="remaja" id="remaja" value="{{$org->remaja}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Keamanan</label>
                            <input class="form-control" name="keamanan" id="keamanan" value="{{$org->keamanan}}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Marbot</label>
                            <input class="form-control" name="marbot" id="marbot" value="{{$org->marbot}}">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection