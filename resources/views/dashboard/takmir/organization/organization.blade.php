@extends('dashboard.master')
@section('title')
Struktur Organisasi
@endsection
@section('css')
<!-- <style>
    .card-title {
        font-size: 10px !important;
    }

    .card-text {
        font-size: 10px !important;
    }

    .card-width {
        width: 150%;
    }

</style> -->
<link rel="stylesheet" href="{{asset('css/structur.css')}}">
@endsection
@section('content')
<?php
    function newline($contain){
        $e = explode(',',$contain);
        $i = implode('<br>',$e);
        return $i;

    }
?>
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin p-0">
    <a class="btn my-3 btn-info" href="{{route('org.edit',$data['org']->uuid)}}"><i class="menu-icon fa fa-edit"></i> Struktur</a>
    <a class="btn my-3 btn-danger float-right" href="{{route('org.editjob',$data['job']->uuid)}}"><i class="menu-icon fa fa-edit"></i> Jobdesc</a>
        <div class="card p-0 scroll">
            <div class="tabel">
                <figure class="org-chart cf">
                    <ul class="table-body">
                        <li>
                            <a href="#">
                                <span class="title">Pelindung</span>
                                <span class="isi-title"><p class="card-text">{!! newline($data['org']->pelindung)  !!}</p></span>
                            </a>

                            <a href="#">
                                <span class="title">Penasehat</span>
                                <span class="isi-title"><p class="card-text">{!! newline($data['org']->penasehat)  !!}</p></span>
                            </a>
                            
                            <a href="#">
                                <span class="title">Ketua</span>
                                <span class="isi-title"><p class="card-text">{!! newline($data['org']->ketua)  !!}</p></span>
                            </a>
                            <span class="garis"></span>
                            <ul class="table-body-bottom cf">
                            <li></li>
                            <li class="list-table dep-a">
                                <a href="#"><span class="title">Bendahara</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->bendahara)  !!}</p></span></a>
                            </li>
                            <li class="list-table dep-b">
                                <a href="#"><span class="title">Sekretaris</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->sekretaris)  !!}</p></span></a>
                            </li>
                            <li class="list-table dep-c">
                                <a href="#"><span class="title">Humas</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->humas)  !!}</p></span></a>
                            </li>
                            <li class="list-table dep-d">
                                <a href="#"><span class="title">Keagamaan</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->keagamaan)  !!}</p></span></a>
                                <ul>
                                    <li>
                                        <a href="#"><span class="title">Tarbiyah</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->tarbiyah)  !!}</p></span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="title">Pengajian</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->pengajian)  !!}</p></span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="title">Imam</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->imam)  !!}</p></span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="title">Muadzin</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->muadzin)  !!}</p></span></a>
                                    </li>
                                </ul>
                            </li>
                            <li class="list-table dep-e">
                                <a href="#"><span class="title">Kepemudaan</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->kepemudaan)  !!}</p></span></a>
                                <ul>
                                    <li><a href="#"><span class="title">Remaja</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->remaja)  !!}</p></span></a></li>
                                </ul>
                            </li>
                            <li class="list-table dep-f">
                                <a href="#"><span class="title">Pelaksana Umum</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->pelaksana_umum)  !!}</p></span></a>
                                <ul>
                                    <li><a href="#"><span class="title">Keamanan</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->keamanan)  !!}</p></span></a></li>
                                    <li><a href="#"><span class="title">Marbot</span><span class="isi-title"><p class="card-text">{!! newline($data['org']->marbot)  !!}</p></span></a></li>
                                </ul>
                            </li>
                            </li>
                        </li>
                    </ul>
                </figure>
            </div>
        </div>
    </div>
</div>
@endsection