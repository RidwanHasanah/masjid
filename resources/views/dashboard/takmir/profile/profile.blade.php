@extends('dashboard.master')
@section('title')
Profile
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <a class="btn btn-warning" href="{{route('profile.edit',$data['user']->uuid)}}"><i class="menu-icon fa fa-edit"></i> Profile</a>
        <a class="btn btn-info float-right" href="{{route('profile.passEdit',$data['user']->uuid)}}"><i class="menu-icon fa fa-edit"></i> Password</a>
        
        <div class="card">
            <div class="card-body card-width">
                <div class="card-title">
                    <h1 class="card-description">Personal Info</h1>
                    <div class="col-md-3">
                        @if (strlen($data['takmir']->photo) != 0)
                        <img class="img-thumbnail d-block" src="{{asset('public/storage/mosque/'.$data['mosque']->photo)}}"
                            id='mosque_img_upload' alt="">
                        @else
                        <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='mosque_img_upload'
                            alt="">
                        @endif
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                {{$data['user']->name}}                                        
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-9">
                                {{$data['takmir']->gender}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                            <div class="col-sm-9">
                                @if(strtotime($data['takmir']->birth_date))
                                    {{ tanggalindonesia(date('Y-m-d',strtotime($data['takmir']->birth_date))) }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Tempat Lahir</label>
                            <div class="col-sm-9">
                                {{ $data['takmir']->birth_place }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">HP</label>
                            <div class="col-sm-9">
                                {{ $data['takmir']->hp }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="province">Provinsi</label>
                            <div class="col-sm-9">
                                {{$data['takmir']->province}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                {{$data['takmir']->address}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body card-width">
                <div class="card-title"> 
                    <h1>Profile Masjid</h1>
                    <br><br>
                    
                    <div class="col-md-3">
                        @if (strlen($data['mosque']->photo) != 0)
                        <img class="img-thumbnail d-block" src="{{asset('public/storage/mosque/'.$data['mosque']->photo)}}"
                            id='mosque_img_upload' alt="">
                        @else
                        <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='mosque_img_upload'
                            alt="">
                        @endif                    
                    </div>
                    <br><br>
    
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="province">Provinsi</label>
                            <div class="col-sm-9">                                    
                                    {{$data['mosque']->province}}                                  
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                    {{$data['mosque']->address}}
                            </div>
                        </div>
                    </div>
    
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Keterangan</label>
                            <div class="col-sm-9">
                                {{$data['mosque']->desc}}
                            </div>
                        </div>
                    </div>
    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection