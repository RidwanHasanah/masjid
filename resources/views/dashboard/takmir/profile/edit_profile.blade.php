@extends('dashboard.master')
@section('title')
Edit Profile
@endsection
@section('content')
{{-- Mosque --}}
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Edit Masjid</h2>
            <form class="form-sample" method="POST" action="{{route('profile.update',$data['user']->uuid)}}" enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('PATCH')}}

                    {{-- Personal Info --}}
                    <div class="row">
                        <div class="col-md-3">
                            @if (strlen($data['mosque']->photo) != 0)
                            <img class="img-thumbnail d-block" src="{{asset('public/storage/mosque/'.$data['mosque']->photo)}}"
                                id='mosque_img_upload' alt="">
                            @else
                            <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='mosque_img_upload'
                                alt="">
                            @endif
                            <br>
                            <div class="input-group mosque_upload">
                                <label class="btn btn-outline-primary shadow" for="mosque_photo">Upload Foto</label>
                                <input class="form-control d-none" value="{{ $data['mosque']->photo }}"
                                    accept="image/jpeg,image/jpg,image/png," type="file" name="mosque_photo" id="mosque_photo">
                                <span class="errorval errorRegis" id="error_photo"></span>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h3 class="card-description">
                                Masjid info
                            </h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Nama Masjid</label>
                                        <div class="col-sm-9">
                                            <input value="{{$data['mosque']->name}}" name="mosque_name" id="mosque_name" type="text"
                                                class="form-control">
                                            @error('mosque_name')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Username Masjid</label>
                                        <div class="col-sm-9">
                                            <input value="{{$data['mosque']->username}}" name="mosque_username" id="mosque_username" type="text"
                                                class="form-control">
                                            @error('mosque_username')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="province">Provinsi</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="mosque_province" id="mosque_province">
                                                <option> </option>
                                                <option {{$data['mosque']->province=='Jakarta'?'selected':''}}
                                                    value="Jakarta">
                                                    Jakarta</option>
                                                <option {{$data['mosque']->province=='Aceh'?'selected':''}}
                                                    value="Aceh">Aceh
                                                </option>
                                                <option {{$data['mosque']->province=='Bali'?'selected':''}}
                                                    value="Bali">Bali
                                                </option>
                                                <option {{$data['mosque']->province=='Banten'?'selected':''}}
                                                    value="Banten">
                                                    Banten</option>
                                                <option {{$data['mosque']->province=='Bengkulu'?'selected':''}}
                                                    value="Bengkulu">Bengkulu</option>
                                                <option {{$data['mosque']->province=='Gorontalo'?'selected':''}}
                                                    value="Gorontalo">Gorontalo</option>
                                                <option {{$data['mosque']->province=='Jambi'?'selected':''}}
                                                    value="Jambi">Jambi
                                                </option>
                                                <option {{$data['mosque']->province=='Jawa Barat'?'selected':''}}
                                                    value="Jawa Barat">Jawa Barat</option>
                                                <option {{$data['mosque']->province=='Jawa Tengah'?'selected':''}}
                                                    value="Jawa Tengah">Jawa Tengah</option>
                                                <option {{$data['mosque']->province=='Jawa Timur'?'selected':''}}
                                                    value="Jawa Timur">Jawa Timur</option>
                                                <option {{$data['mosque']->province=='Kalimantan Barat'?'selected':''}}
                                                    value="Kalimantan Barat">Kalimantan Barat</option>
                                                <option
                                                    {{$data['mosque']->province=='Kalimantan Selatan'?'selected':''}}
                                                    value="Kalimantan Selatan">Kalimantan Selatan</option>
                                                <option {{$data['mosque']->province=='Kalimantan Tengah'?'selected':''}}
                                                    value="Kalimantan Tengah">Kalimantan Tengah</option>
                                                <option {{$data['mosque']->province=='Kalimantan Timur'?'selected':''}}
                                                    value="Kalimantan Timur">Kalimantan Timur</option>
                                                <option {{$data['mosque']->province=='Kalimantan Utara'?'selected':''}}
                                                    value="Kalimantan Utara">Kalimantan Utara</option>
                                                <option
                                                    {{$data['mosque']->province=='Kepulauan Bangka Belitung'?'selected':''}}
                                                    value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung
                                                </option>
                                                <option {{$data['mosque']->province=='Kepulauan Riau'?'selected':''}}
                                                    value="Kepulauan Riau">Kepulauan Riau</option>
                                                <option {{$data['mosque']->province=='Lampung'?'selected':''}}
                                                    value="Lampung">
                                                    Lampung</option>
                                                <option {{$data['mosque']->province=='Maluku'?'selected':''}}
                                                    value="Maluku">
                                                    Maluku</option>
                                                <option {{$data['mosque']->province=='Maluku Utara'?'selected':''}}
                                                    value="Maluku Utara">Maluku Utara</option>
                                                <option
                                                    {{$data['mosque']->province=='Nusa Tenggara Barat'?'selected':''}}
                                                    value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                                <option
                                                    {{$data['mosque']->province=='Nusa Tenggara Timur'?'selected':''}}
                                                    value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                                <option {{$data['mosque']->province=='Papua'?'selected':''}}
                                                    value="Papua">Papua
                                                </option>
                                                <option {{$data['mosque']->province=='Papua Barat'?'selected':''}}
                                                    value="Papua Barat">Papua Barat</option>
                                                <option {{$data['mosque']->province=='Riau'?'selected':''}}
                                                    value="Riau">Riau
                                                </option>
                                                <option {{$data['mosque']->province=='Sulawesi Barat'?'selected':''}}
                                                    value="Sulawesi Barat">Sulawesi Barat</option>
                                                <option {{$data['mosque']->province=='Sulawesi Selatan'?'selected':''}}
                                                    value="Sulawesi Selatan">Sulawesi Selatan</option>
                                                <option {{$data['mosque']->province=='Sulawesi Tengah'?'selected':''}}
                                                    value="Sulawesi Tengah">Sulawesi Tengah</option>
                                                <option {{$data['mosque']->province=='Sulawesi Tenggara'?'selected':''}}
                                                    value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                                <option {{$data['mosque']->province=='Sulawesi Utara'?'selected':''}}
                                                    value="Sulawesi Utara">Sulawesi Utara</option>
                                                <option {{$data['mosque']->province=='Sumatera Barat'?'selected':''}}
                                                    value="Sumatera Barat">Sumatera Barat</option>
                                                <option {{$data['mosque']->province=='Sumatera Selatan'?'selected':''}}
                                                    value="Sumatera Selatan">Sumatera Selatan</option>
                                                <option {{$data['mosque']->province=='Sumatera Utara'?'selected':''}}
                                                    value="Sumatera Utara">Sumatera Utara</option>
                                                <option {{$data['mosque']->province=='Yogyakarta'?'selected':''}}
                                                    value="Yogyakarta">Yogyakarta</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <textarea name="mosque_address" type="text"
                                            class="form-control">{{$data['mosque']->address}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Google Maps <br> <br>
                                            <a href="/help" style="font-size:x-small" class="text-black col-sm-3 col-form-label">Tutorial</a>
                                        </label>
                                        <div class="col-sm-9">
                                            <textarea name="mosque_maps" type="text"
                                            class="form-control">{{$data['mosque']->gmaps}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Visi</label>
                                        <div class="col-sm-9">
                                            <textarea name="mosque_visi" type="text"
                                                class="form-control">{{$data['mosque']->visi}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Misi</label>
                                        <div class="col-sm-9">
                                            <textarea name="mosque_misi" type="text"
                                                class="form-control">{{$data['mosque']->misi}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Keterangan</label>
                                        <div class="col-sm-9">
                                            <textarea name="mosque_desc" type="text"
                                                class="form-control">{{$data['mosque']->desc}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>


        {{-- Takmir --}}


        <div class="card">
            <hr>
            <div class="card-body">
                <h2 class="card-title">Edit Profile</h2>

                {{-- Personal Info --}}
                <div class="row">
                    <div class="col-md-3">
                        @if (strlen($data['takmir']->photo) != 0)
                        <img class="img-thumbnail d-block" src="{{asset('public/storage/takmir/'.$data['takmir']->photo)}}"
                            id='img-upload' alt="">
                        @else
                        <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                        @endif
                        <br>
                        <div class="input-group upload">
                            <label class="btn btn-outline-primary shadow" for="photo">Upload Foto</label>
                            <input class="form-control d-none" value="{{ $data['takmir']->photo }}"
                                accept="image/jpeg,image/jpg,image/png," type="file" name="photo" id="photo">
                            <span class="errorval errorRegis" id="error_photo"></span>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <h3 class="card-description">
                            Personal info
                        </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Name</label>
                                    <div class="col-sm-9">
                                        <input value="{{$data['user']->name}}" name="name" id="name" type="text"
                                            class="form-control">
                                        @error('name')
                                        <span class="mt-1 alert-danger" role="alert">
                                            <small>{{ $message }}</small>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-9">
                                        <select name="gender" class="form-control">
                                            <option {{$data['takmir']->gender=='Pria'?'selected':''}} value="Pria">Pria
                                            </option>
                                            <option {{$data['takmir']->gender=='Wanita'?'selected':''}} value="Wanita">
                                                Wanita</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-9">
                                        <input value="{{ $data['takmir']->birth_date }}" style="cursor:pointer" readonly
                                            name="birth_date" id="datepicker" class="form-control"
                                            placeholder="dd-mm-yyyy">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-9">
                                        <input value="{{ $data['takmir']->birth_place }}" style="cursor:pointer"
                                            name="birth_place" id="birth_place" class="form-control" placeholder=" ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">HP</label>
                                    <div class="col-sm-9">
                                        <input value="{{ $data['takmir']->hp }}" name="hp" type="text"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="province">Provinsi</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="province" id="province">
                                            <option> </option>
                                            <option {{$data['takmir']->province=='Jakarta'?'selected':''}}
                                                value="Jakarta">
                                                Jakarta</option>
                                            <option {{$data['takmir']->province=='Aceh'?'selected':''}} value="Aceh">
                                                Aceh
                                            </option>
                                            <option {{$data['takmir']->province=='Bali'?'selected':''}} value="Bali">
                                                Bali
                                            </option>
                                            <option {{$data['takmir']->province=='Banten'?'selected':''}}
                                                value="Banten">
                                                Banten</option>
                                            <option {{$data['takmir']->province=='Bengkulu'?'selected':''}}
                                                value="Bengkulu">Bengkulu</option>
                                            <option {{$data['takmir']->province=='Gorontalo'?'selected':''}}
                                                value="Gorontalo">Gorontalo</option>
                                            <option {{$data['takmir']->province=='Jambi'?'selected':''}} value="Jambi">
                                                Jambi
                                            </option>
                                            <option {{$data['takmir']->province=='Jawa Barat'?'selected':''}}
                                                value="Jawa Barat">Jawa Barat</option>
                                            <option {{$data['takmir']->province=='Jawa Tengah'?'selected':''}}
                                                value="Jawa Tengah">Jawa Tengah</option>
                                            <option {{$data['takmir']->province=='Jawa Timur'?'selected':''}}
                                                value="Jawa Timur">Jawa Timur</option>
                                            <option {{$data['takmir']->province=='Kalimantan Barat'?'selected':''}}
                                                value="Kalimantan Barat">Kalimantan Barat</option>
                                            <option {{$data['takmir']->province=='Kalimantan Selatan'?'selected':''}}
                                                value="Kalimantan Selatan">Kalimantan Selatan</option>
                                            <option {{$data['takmir']->province=='Kalimantan Tengah'?'selected':''}}
                                                value="Kalimantan Tengah">Kalimantan Tengah</option>
                                            <option {{$data['takmir']->province=='Kalimantan Timur'?'selected':''}}
                                                value="Kalimantan Timur">Kalimantan Timur</option>
                                            <option {{$data['takmir']->province=='Kalimantan Utara'?'selected':''}}
                                                value="Kalimantan Utara">Kalimantan Utara</option>
                                            <option
                                                {{$data['takmir']->province=='Kepulauan Bangka Belitung'?'selected':''}}
                                                value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung
                                            </option>
                                            <option {{$data['takmir']->province=='Kepulauan Riau'?'selected':''}}
                                                value="Kepulauan Riau">Kepulauan Riau</option>
                                            <option {{$data['takmir']->province=='Lampung'?'selected':''}}
                                                value="Lampung">
                                                Lampung</option>
                                            <option {{$data['takmir']->province=='Maluku'?'selected':''}}
                                                value="Maluku">
                                                Maluku</option>
                                            <option {{$data['takmir']->province=='Maluku Utara'?'selected':''}}
                                                value="Maluku Utara">Maluku Utara</option>
                                            <option {{$data['takmir']->province=='Nusa Tenggara Barat'?'selected':''}}
                                                value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                            <option {{$data['takmir']->province=='Nusa Tenggara Timur'?'selected':''}}
                                                value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                            <option {{$data['takmir']->province=='Papua'?'selected':''}} value="Papua">
                                                Papua
                                            </option>
                                            <option {{$data['takmir']->province=='Papua Barat'?'selected':''}}
                                                value="Papua Barat">Papua Barat</option>
                                            <option {{$data['takmir']->province=='Riau'?'selected':''}} value="Riau">
                                                Riau
                                            </option>
                                            <option {{$data['takmir']->province=='Sulawesi Barat'?'selected':''}}
                                                value="Sulawesi Barat">Sulawesi Barat</option>
                                            <option {{$data['takmir']->province=='Sulawesi Selatan'?'selected':''}}
                                                value="Sulawesi Selatan">Sulawesi Selatan</option>
                                            <option {{$data['takmir']->province=='Sulawesi Tengah'?'selected':''}}
                                                value="Sulawesi Tengah">Sulawesi Tengah</option>
                                            <option {{$data['takmir']->province=='Sulawesi Tenggara'?'selected':''}}
                                                value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                            <option {{$data['takmir']->province=='Sulawesi Utara'?'selected':''}}
                                                value="Sulawesi Utara">Sulawesi Utara</option>
                                            <option {{$data['takmir']->province=='Sumatera Barat'?'selected':''}}
                                                value="Sumatera Barat">Sumatera Barat</option>
                                            <option {{$data['takmir']->province=='Sumatera Selatan'?'selected':''}}
                                                value="Sumatera Selatan">Sumatera Selatan</option>
                                            <option {{$data['takmir']->province=='Sumatera Utara'?'selected':''}}
                                                value="Sumatera Utara">Sumatera Utara</option>
                                            <option {{$data['takmir']->province=='Yogyakarta'?'selected':''}}
                                                value="Yogyakarta">Yogyakarta</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Alamat Lengkap</label>
                                    <div class="col-sm-9">
                                        <textarea name="address" type="text"
                                            class="form-control">{{$data['takmir']->address}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary btn-block" value="Edit">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
@endsection