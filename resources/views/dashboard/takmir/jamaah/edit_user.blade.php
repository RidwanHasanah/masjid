@extends('dashboard.master')
@section('title')
    Edit Jamaah
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Edit Jamaah</h2>
                <form class="form-sample" method="POST" action="{{route('jamaah.update',$jamaah->uuid)}}"
                    enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('PATCH')}}

                    {{-- Personal Info --}}
                    <div class="row">
                        <div class="col-md-3">
                            @if (strlen($jamaah->photo) != 0)
                            <img class="img-thumbnail d-block" src="{{asset('public/storage/jamaah/'.$jamaah->photo)}}" id='img-upload' alt="">
                            @else
                            <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                            @endif
                            <br>
                            <div class="input-group upload">
                                <label class="btn btn-outline-primary shadow" for="photo">Upload Foto</label>
                                <input class="form-control d-none" value="{{ $jamaah->photo }}"
                                    accept="image/jpeg,image/jpg,image/png," type="file" name="photo" id="photo">
                                <span class="errorval errorRegis" id="error_photo"></span>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h3 class="card-description">
                                Personal info
                            </h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input value="{{$jamaah->name}}" name="name" id="name" type="text"
                                                class="form-control">
                                            @error('name')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                        <div class="col-sm-9">
                                            <select name="gender" class="form-control">
                                                <option {{$jamaah->gender=='Pria'?'selected':''}} value="Pria">Pria
                                                </option>
                                                <option {{$jamaah->gender=='Wanita'?'selected':''}} value="Wanita">
                                                    Wanita</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $jamaah->birth_date }}" style="cursor:pointer" readonly
                                                name="birth_date" id="datepicker" class="form-control"
                                                placeholder="dd-mm-yyyy">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Tempat Lahir</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $jamaah->birth_place }}" style="cursor:pointer"
                                                name="birth_place" id="birth_place" class="form-control"
                                                placeholder=" ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">HP</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $jamaah->hp }}" name="hp" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="province">Provinsi</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="province" id="province">
                                                <option>  </option>
                                                <option {{$jamaah->province=='Jakarta'?'selected':''}} value="Jakarta">
                                                    Jakarta</option>
                                                <option {{$jamaah->province=='Aceh'?'selected':''}} value="Aceh">Aceh
                                                </option>
                                                <option {{$jamaah->province=='Bali'?'selected':''}} value="Bali">Bali
                                                </option>
                                                <option {{$jamaah->province=='Banten'?'selected':''}} value="Banten">
                                                    Banten</option>
                                                <option {{$jamaah->province=='Bengkulu'?'selected':''}}
                                                    value="Bengkulu">Bengkulu</option>
                                                <option {{$jamaah->province=='Gorontalo'?'selected':''}}
                                                    value="Gorontalo">Gorontalo</option>
                                                <option {{$jamaah->province=='Jambi'?'selected':''}} value="Jambi">Jambi
                                                </option>
                                                <option {{$jamaah->province=='Jawa Barat'?'selected':''}}
                                                    value="Jawa Barat">Jawa Barat</option>
                                                <option {{$jamaah->province=='Jawa Tengah'?'selected':''}}
                                                    value="Jawa Tengah">Jawa Tengah</option>
                                                <option {{$jamaah->province=='Jawa Timur'?'selected':''}}
                                                    value="Jawa Timur">Jawa Timur</option>
                                                <option {{$jamaah->province=='Kalimantan Barat'?'selected':''}}
                                                    value="Kalimantan Barat">Kalimantan Barat</option>
                                                <option {{$jamaah->province=='Kalimantan Selatan'?'selected':''}}
                                                    value="Kalimantan Selatan">Kalimantan Selatan</option>
                                                <option {{$jamaah->province=='Kalimantan Tengah'?'selected':''}}
                                                    value="Kalimantan Tengah">Kalimantan Tengah</option>
                                                <option {{$jamaah->province=='Kalimantan Timur'?'selected':''}}
                                                    value="Kalimantan Timur">Kalimantan Timur</option>
                                                <option {{$jamaah->province=='Kalimantan Utara'?'selected':''}}
                                                    value="Kalimantan Utara">Kalimantan Utara</option>
                                                <option {{$jamaah->province=='Kepulauan Bangka Belitung'?'selected':''}}
                                                    value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung
                                                </option>
                                                <option {{$jamaah->province=='Kepulauan Riau'?'selected':''}}
                                                    value="Kepulauan Riau">Kepulauan Riau</option>
                                                <option {{$jamaah->province=='Lampung'?'selected':''}} value="Lampung">
                                                    Lampung</option>
                                                <option {{$jamaah->province=='Maluku'?'selected':''}} value="Maluku">
                                                    Maluku</option>
                                                <option {{$jamaah->province=='Maluku Utara'?'selected':''}}
                                                    value="Maluku Utara">Maluku Utara</option>
                                                <option {{$jamaah->province=='Nusa Tenggara Barat'?'selected':''}}
                                                    value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                                <option {{$jamaah->province=='Nusa Tenggara Timur'?'selected':''}}
                                                    value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                                <option {{$jamaah->province=='Papua'?'selected':''}} value="Papua">Papua
                                                </option>
                                                <option {{$jamaah->province=='Papua Barat'?'selected':''}}
                                                    value="Papua Barat">Papua Barat</option>
                                                <option {{$jamaah->province=='Riau'?'selected':''}} value="Riau">Riau
                                                </option>
                                                <option {{$jamaah->province=='Sulawesi Barat'?'selected':''}}
                                                    value="Sulawesi Barat">Sulawesi Barat</option>
                                                <option {{$jamaah->province=='Sulawesi Selatan'?'selected':''}}
                                                    value="Sulawesi Selatan">Sulawesi Selatan</option>
                                                <option {{$jamaah->province=='Sulawesi Tengah'?'selected':''}}
                                                    value="Sulawesi Tengah">Sulawesi Tengah</option>
                                                <option {{$jamaah->province=='Sulawesi Tenggara'?'selected':''}}
                                                    value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                                <option {{$jamaah->province=='Sulawesi Utara'?'selected':''}}
                                                    value="Sulawesi Utara">Sulawesi Utara</option>
                                                <option {{$jamaah->province=='Sumatera Barat'?'selected':''}}
                                                    value="Sumatera Barat">Sumatera Barat</option>
                                                <option {{$jamaah->province=='Sumatera Selatan'?'selected':''}}
                                                    value="Sumatera Selatan">Sumatera Selatan</option>
                                                <option {{$jamaah->province=='Sumatera Utara'?'selected':''}}
                                                    value="Sumatera Utara">Sumatera Utara</option>
                                                <option {{$jamaah->province=='Yogyakarta'?'selected':''}}
                                                    value="Yogyakarta">Yogyakarta</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <textarea name="address" type="text"
                                                class="form-control">{{$jamaah->address}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary btn-block" value="Edit">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
@endsection