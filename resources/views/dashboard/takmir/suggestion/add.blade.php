@extends('dashboard.master')
@section('title')
Kritik & Saran
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Kritik & Saran</h2>
                <form class="form-sample" method="POST" action="{{route('saran.store')}}"
                    enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('POST')}}

                    {{-- Personal Info --}}
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Subject</label>
                                        <div class="col-sm-10">
                                            <input value="{{ old('subject') }}" name="subject" id="subject" type="text"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Pesan</label>
                                        <div class="col-sm-10">
                                            <textarea name="pesan" id="pesan" cols="10" rows="20" class="form-control">{{ old('pesan') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary btn-block" value="Kirim">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
@endsection