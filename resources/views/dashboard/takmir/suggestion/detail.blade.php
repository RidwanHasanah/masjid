@extends('dashboard.master')
@section('title')
    Kritik & Saran
@endsection
@section('content')
<div class="container-fluid p-5 pt-max bg-grad" height="50%">
    <div class="row">
        <div class="col-md-12">
            <div class="row text-main ">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="text-black bg-light">Kritik & Saran</h1>
                        </div>
                        <div class="card-body">
                            <h6 class="card-title text-main">Dari: <br> {{$suggest->name}} 
                                <br> Masjid {{$suggest->masjid}} 
                                <br> {{$suggest->address}} 
                                <br> {{tanggalindonesia(date('Y-m-d',strtotime($suggest->suggest_at)))}}
                            </h6> 
                            <br>
                            <h1 class="card-title text-main">Subject: <br> {{ $suggest->subject }}</h1>
                            <p class="card-text">Pesan: <br> {{$suggest->suggest}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection