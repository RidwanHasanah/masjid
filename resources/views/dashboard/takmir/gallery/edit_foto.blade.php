@extends('dashboard.master')
@section('title')
Edit Foto
@endsection
@section('content')
    <div class="row">
    @include('layouts.patrials.alerts')
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Edit Foto</h2>
                    <form action="{{ route('update.foto', $data['gallery']->slug) }}" class="form-sample" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('PUT')}}

                        <div class="row">
                            <div class="col-md-12">
                                <div class="row justify-content-center">
                                    <div class="col-md-12">
                                        <div class="col-md-5 ">
                                            @if (strlen($data['gallery']->photo) != 0)
                                            <img class="img-thumbnail d-block" src="{{asset('storage/gallery/'.$data['gallery']->photo)}}"
                                                id='img-upload' alt="">
                                            @else
                                            <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                                            @endif
                                            <br>
                                            <div class="input-group upload">
                                                <label class="btn btn-outline-primary shadow" for="photo">Upload Foto</label>
                                                <input class="form-control d-none" value="{{ $data['gallery']->photo }}"
                                                    accept="image/jpeg,image/jpg,image/png," type="file" name="photo" id="photo">
                                                <!-- <span class="errorval errorRegis" id="error_photo"></span> -->
                                                @error('photo')
                                                <span class="mt-1 alert-danger" role="alert">
                                                    <small>Photo Ukuran Maksimal: 500kb... Harap periksa kembali Ukuran Photo anda</small>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary btn-block" value="Simpan">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection