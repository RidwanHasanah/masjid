@foreach($album as $albums)
@extends('dashboard.master')
@section('title')
    {{$albums->name}}
@endsection
@section('content')
<div class="container-fluid p-5 pt-max bg-grad" height="50%">
    @include('layouts.patrials.alerts')
    <div class="card">
        <a class="btn btn-primary float-right" href="{{ route('create.foto', $albums->uuid) }}">Tambah Foto</a>
        <div class="card-body">
            <div class="card-header text-center">
                <h2 class="text-center">Album {{$albums->name}}</h2> 
            </div> <br>
            
            <div class="row  album">
                @foreach($album as $albums) 
                <div class="col-md-4">
                    <div class="card foto">
                        <div class="card-img">

                            
                            <a href="{{ asset('storage/gallery/'.$albums->photo) }}" class="image-popup to-animate fadeInUp animated">
                                @if (strlen($albums->photo) != 0)
                                <img width="100%" class="img-thumbnail d-block img-responsive" src="{{ asset('storage/gallery/'.$albums->photo)}}" id='img-upload' alt="Image">
                                @else
                                <img width="100%" class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                                @endif
                            </a>
                        </div>

                        <div class="card-text">
                            <a href="{{ route('edit.foto', $albums->slug_foto) }}" style='margin-top:0.5em; width:6em;' class='btn btn-success btn-outline btn-xs text-white float-left'><i class='fa fa-pencil'></i></a>
                            <a href="" style='margin-top:0.5em; width:6em;' class='btn btn-danger btn-outline btn-xs text-white float-right'><i class='fa fa-trash'></i></a>
                        </div>

                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
@endforeach