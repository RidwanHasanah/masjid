@extends('dashboard.master')
@section('title')
Edit Album
@endsection
@section('content')
    <div class="row">
    @include('layouts.patrials.alerts')
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Edit Album</h2>
                    <form action="{{ route('gallery.update', $data['album']->uuid) }}" class="form-sample" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('PATCH')}}

                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                         <div class="form-group row">
                                         <label class="col-sm-3 col-form-label">Album</label>
                                            <div class="col-sm-9">
                                                <input value="{{ $data['album']->name }}" name="album" id="album" type="text"
                                                    class="form-control">
                                                @error('album')
                                                <span class="mt-1 alert-danger" role="alert">
                                                    <small>{{ $message }}</small>
                                                </span>
                                                @enderror
                                            </div>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary btn-block" value="Simpan">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection