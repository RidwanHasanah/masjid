@extends('dashboard.master')
@section('title')
Tambah Gallery
@endsection
@section('content')
    <div class="row">
    @include('layouts.patrials.alerts')
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Tambah Gallery</h2>
                    <form action="{{ route('gallery.store') }}" class="form-sample" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('POST')}}

                        <div class="row">
                            <div class="col-md-5">
                                <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                                <br>
                                <div class="input-group upload">
                                    <label class="btn btn-outline-primary shadow" for="photo">Upload Foto</label>
                                    <input class="form-control d-none" value="{{ old('photo') }}"
                                        accept="image/jpeg,image/jpg,image/png," type="file" name="photo" id="photo">
                                    <!-- <span class="errorval errorRegis" id="error_photo"></span> -->
                                    @error('photo')
                                    <span class="mt-1 alert-danger" role="alert">
                                        <small>Photo Ukuran Maksimal: 500kb... Harap periksa kembali Ukuran Photo anda</small>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-12">
                                         <div class="form-group row">
                                         <label class="col-sm-3 col-form-label">Album</label>
                                            <div class="col-sm-9">
                                                <input value="{{ old('album') }}" name="album" id="album" type="text"
                                                    class="form-control">
                                                @error('album')
                                                <span class="mt-1 alert-danger" role="alert">
                                                    <small>{{ $message }}</small>
                                                </span>
                                                @enderror
                                            </div>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary btn-block" value="Simpan">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection