@extends('dashboard.master')
@section('title')
Semua Album
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                @include('layouts.patrials.alerts')
                <div class="row">
                    <div class="col-md-6"><h4 class="card-title"> Semua Album </h4></div>
                    <div class="col-md-6"><a class="btn btn-primary float-right" href="{{route('gallery.create')}}">Tambah Gallery</a></div>
                </div>
                <div class="center-pos" id="loadingDiv">
                        <img class="loading" src="{{asset('img/loading.gif')}}">
                </div>
                <div class="table-responsive">
                    <table id="table-gallery" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nama Album</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">
    
    var ajaxApi = '{{route("api.gallery")}}'
    var table = $('#table-gallery').DataTable({
        order: [[1,'desc']],
        processing: true,
        serverSide: true,
        ajax : ajaxApi,
        columns : [
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action',ordertable: false, searchable:false},
        ]

    })

    
    

    // Delete Data
    function deleteAlbum(id){
        // alert('bismillah');
        
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        Swal.fire({
          title: 'Kamu yakin ?',
          text: "Album yang di hapus tidak akan kembali!",
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Ya, Hapus ini!'
        }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "{{ url('gallery') }}" + '/' + id,
            type: "DELETE",
            data: {'_method' : 'DELETE', '_token' : csrf_token},
            success: function(data){
              table.ajax.reload();
              Swal.fire({
                title:'Berhasil',
                text: 'Album Sudah di hapus',
                type: 'success',
                timer: '1500'
              })
            },
            error: function(){
              Swal.fire({
                title: 'Oops',
                text: 'Something went wrong',
                type: 'error',
                timer: '1500'
              })
            }
          })
        }
      })
    }
    </script>
@endsection