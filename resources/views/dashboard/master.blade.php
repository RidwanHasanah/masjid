<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="{{asset('dashboard/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('dashboard/vendors/iconfonts/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('dashboard/vendors/css/vendor.bundle.base.css')}}">
        <link rel="stylesheet" href="{{asset('dashboard/vendors/css/vendor.bundle.addons.css')}}">
        <!-- endinject -->
        <!-- plugin css for this page -->
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="{{asset('dashboard/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
        <!-- endinject -->
        <link rel="shortcut icon" href="{{asset('dashboard/images/favicon.png')}}" />
        {{-- Dashboard --}}
        <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
        <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">

        	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="{{asset('frontpage/main/css/simple-line-icons.css')}}">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{asset('frontpage/main/css/magnific-popup.css')}}">
        
        {{-- <link rel="stylesheet" href="{{asset('sweetalert2/sweetalert2.css')}}"> --}}
        @yield('css')
      </head>

<body>
  <div class="container-scroller">
    @include('dashboard.components.nav')
    <div class="container-fluid page-body-wrapper">
      @include('dashboard.components.menu')
      <div class="main-panel">
        <div class="content-wrapper">
            @yield('content')
        </div>
      @include('dashboard.components.footer')
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
{{-- Jquery --}}
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
{{-- Dashboard --}}
<script src="{{asset('js/dashboard.js')}}"></script>
{{-- <script src="{{asset('sweetalert2/sweetalert2.js')}}"></script> --}}
  <!-- plugins:js -->
  <script src="{{asset('dashboard/vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{asset('dashboard/vendors/js/vendor.bundle.addons.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{asset('dashboard/js/off-canvas.js')}}"></script>
  <script src="{{asset('dashboard/js/misc.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('dashboard/js/dashboard.js')}}"></script>
  <!-- End custom js for this page-->
  {{-- CKEditor --}}
  <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
  <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
  	
	<!-- Waypoints -->
	<script src="{{asset('frontpage/main/js/jquery.waypoints.min.js')}}"></script>
	<!-- Stellar Parallax -->
	<script src="{{asset('frontpage/main/js/jquery.stellar.min.js')}}"></script>
	<!-- Counter -->
	<script src="{{asset('frontpage/main/js/jquery.countTo.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{asset('frontpage/main/js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{asset('frontpage/main/js/magnific-popup-options.js')}}"></script>
  <script type="text/javascript">
      $('.wysiwyg').ckeditor();
  </script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  
@yield('js')

</body>

</html>