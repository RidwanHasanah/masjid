@extends('dashboard.master')
@section('title')
    Info Takmir
@endsection
@section('content')
<div class="row">
  @include('layouts.patrials.alerts')
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="card-title">
          <h5 class="">Info Takmir</h5>
        </div>
        <div class="">
          <div class="row">
            <div class="col-md-3 col-lg-3 " align="center">
                @if (strlen($takmir->photo) != 0)
                <img alt="User Pic" src="{{asset('public/storage/user/'.$takmir->photo)}}" class="img-circle img-responsive img-thumbnail">        
                @else
                <img alt="User Pic" src="{{asset('img/user.png')}}" class="img-circle img-responsive img-thumbnail"> 
                @endif
            </div>
            <div class="table-responsive col-md-9 col-lg-9 ">
              <table class="table table-striped table-hover">
                <tbody>
                  <tr>
                    <td><b>Nama </b></td>
                    <td>{{$user->name}}</td>
                  </tr>
                  <tr>
                    <td><b>Tempat, Tanggal Lahir</b></td>
                    <td>{{$takmir->birth_date}} , {{$takmir->birth_place}}</td>
                  </tr>
                  <tr>
                    <td><b>Jenis Kelamin</b></td>
                    <td>{{$takmir->gender}}</td>
                  </tr>
                  <tr>
                    <td><b>Provinsi</b></td>
                    <td>{{$takmir->province}}</td>
                  </tr>
                  <tr>
                    <td><b>Alamat</b></td>
                    <td>{{$takmir->address}}</td>
                  </tr>
                  <tr>
                    <td><b>Email</b></td>
                    <td><a href="{{$user->email}}">{{$user->email}}</a></td>
                  </tr>
                  <tr>
                    <td><b>Nomor HandPhone</b></td>
                    <td>{{$takmir->hp}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="card-title">
          <h5 class="">Info Masjid</h5>
        </div>
        <div class="">
          <div class="row">
            <div class="col-md-3 col-lg-3 " align="center">
                @if (strlen($mosque->photo) != 0)
                <img alt="User Pic" src="{{asset('public/storage/mosque/'.$mosque->photo)}}" class="img-circle img-responsive img-thumbnail">        
                @else
                <img alt="User Pic" src="{{asset('img/noimage.png')}}" class="img-circle img-responsive img-thumbnail"> 
                @endif
            </div>
            <div class="table-responsive col-md-9 col-lg-9 ">
              <table class="table table-striped table-hover">
                <tbody>
                  <tr>
                    <td><b>Nama Masjid</b></td>
                    <td>Masjid {{$mosque->name}}</td>
                  </tr>
                  <tr>
                    <td><b>Provinsi</b></td>
                    <td>{{$mosque->province}}</td>
                  </tr>
                  <tr>
                    <td><b>Alamat</b></td>
                    <td>{{$mosque->address}}</td>
                  </tr>
                  <tr>
                    <td><b>Keterangan</b></td>
                    <td>{{$mosque->desc}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection