@extends('dashboard.master')
@section('title')
    Edit Takmir
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Edit User</h2>
                <form class="form-sample" method="POST" action="{{route('takmir.update',$user->uuid)}}"
                    enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('PATCH')}}

                    {{-- Personal Info --}}
                    <div class="row">
                        <div class="col-md-3">
                            @if (strlen($takmir->photo) != 0)
                            <img class="img-thumbnail d-block" src="{{asset('public/storage/user/'.$takmir->photo)}}" id='img-upload' alt="">
                            @else
                            <img class="img-thumbnail d-block" src="{{asset('img/user.png')}}" id='img-upload' alt="">
                            @endif
                            <br>
                            <div class="input-group upload">
                                <label class="btn btn-outline-primary shadow" for="photo">Upload Foto</label>
                                <input class="form-control d-none" value="{{ $takmir->photo }}"
                                    accept="image/jpeg,image/jpg,image/png," type="file" name="photo" id="photo">
                                <span class="errorval errorRegis" id="error_photo"></span>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h3 class="card-description">
                                Personal info
                            </h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input value="{{$user->name}}" name="name" id="name" type="text"
                                                class="form-control">
                                            @error('name')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Email</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $user->email }}" name="email" id="email" type="text"
                                                class="form-control">
                                            @error('email')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                        <div class="col-sm-9">
                                            <select name="gender" class="form-control">
                                                <option {{$takmir->gender=='Pria'?'selected':''}} value="Pria">Pria
                                                </option>
                                                <option {{$takmir->gender=='Wanita'?'selected':''}} value="Wanita">
                                                    Wanita</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $takmir->birth_date }}" style="cursor:pointer" readonly
                                                name="birth_date" id="datepicker" class="form-control"
                                                placeholder="dd-mm-yyyy">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <textarea name="address" type="text"
                                                class="form-control">{{$takmir->address}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="province">Provinsi</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="province" id="province">
                                                <option>  </option>
                                                <option {{$takmir->province=='Jakarta'?'selected':''}} value="Jakarta">
                                                    Jakarta</option>
                                                <option {{$takmir->province=='Aceh'?'selected':''}} value="Aceh">Aceh
                                                </option>
                                                <option {{$takmir->province=='Bali'?'selected':''}} value="Bali">Bali
                                                </option>
                                                <option {{$takmir->province=='Banten'?'selected':''}} value="Banten">
                                                    Banten</option>
                                                <option {{$takmir->province=='Bengkulu'?'selected':''}}
                                                    value="Bengkulu">Bengkulu</option>
                                                <option {{$takmir->province=='Gorontalo'?'selected':''}}
                                                    value="Gorontalo">Gorontalo</option>
                                                <option {{$takmir->province=='Jambi'?'selected':''}} value="Jambi">Jambi
                                                </option>
                                                <option {{$takmir->province=='Jawa Barat'?'selected':''}}
                                                    value="Jawa Barat">Jawa Barat</option>
                                                <option {{$takmir->province=='Jawa Tengah'?'selected':''}}
                                                    value="Jawa Tengah">Jawa Tengah</option>
                                                <option {{$takmir->province=='Jawa Timur'?'selected':''}}
                                                    value="Jawa Timur">Jawa Timur</option>
                                                <option {{$takmir->province=='Kalimantan Barat'?'selected':''}}
                                                    value="Kalimantan Barat">Kalimantan Barat</option>
                                                <option {{$takmir->province=='Kalimantan Selatan'?'selected':''}}
                                                    value="Kalimantan Selatan">Kalimantan Selatan</option>
                                                <option {{$takmir->province=='Kalimantan Tengah'?'selected':''}}
                                                    value="Kalimantan Tengah">Kalimantan Tengah</option>
                                                <option {{$takmir->province=='Kalimantan Timur'?'selected':''}}
                                                    value="Kalimantan Timur">Kalimantan Timur</option>
                                                <option {{$takmir->province=='Kalimantan Utara'?'selected':''}}
                                                    value="Kalimantan Utara">Kalimantan Utara</option>
                                                <option {{$takmir->province=='Kepulauan Bangka Belitung'?'selected':''}}
                                                    value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung
                                                </option>
                                                <option {{$takmir->province=='Kepulauan Riau'?'selected':''}}
                                                    value="Kepulauan Riau">Kepulauan Riau</option>
                                                <option {{$takmir->province=='Lampung'?'selected':''}} value="Lampung">
                                                    Lampung</option>
                                                <option {{$takmir->province=='Maluku'?'selected':''}} value="Maluku">
                                                    Maluku</option>
                                                <option {{$takmir->province=='Maluku Utara'?'selected':''}}
                                                    value="Maluku Utara">Maluku Utara</option>
                                                <option {{$takmir->province=='Nusa Tenggara Barat'?'selected':''}}
                                                    value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                                <option {{$takmir->province=='Nusa Tenggara Timur'?'selected':''}}
                                                    value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                                <option {{$takmir->province=='Papua'?'selected':''}} value="Papua">Papua
                                                </option>
                                                <option {{$takmir->province=='Papua Barat'?'selected':''}}
                                                    value="Papua Barat">Papua Barat</option>
                                                <option {{$takmir->province=='Riau'?'selected':''}} value="Riau">Riau
                                                </option>
                                                <option {{$takmir->province=='Sulawesi Barat'?'selected':''}}
                                                    value="Sulawesi Barat">Sulawesi Barat</option>
                                                <option {{$takmir->province=='Sulawesi Selatan'?'selected':''}}
                                                    value="Sulawesi Selatan">Sulawesi Selatan</option>
                                                <option {{$takmir->province=='Sulawesi Tengah'?'selected':''}}
                                                    value="Sulawesi Tengah">Sulawesi Tengah</option>
                                                <option {{$takmir->province=='Sulawesi Tenggara'?'selected':''}}
                                                    value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                                <option {{$takmir->province=='Sulawesi Utara'?'selected':''}}
                                                    value="Sulawesi Utara">Sulawesi Utara</option>
                                                <option {{$takmir->province=='Sumatera Barat'?'selected':''}}
                                                    value="Sumatera Barat">Sumatera Barat</option>
                                                <option {{$takmir->province=='Sumatera Selatan'?'selected':''}}
                                                    value="Sumatera Selatan">Sumatera Selatan</option>
                                                <option {{$takmir->province=='Sumatera Utara'?'selected':''}}
                                                    value="Sumatera Utara">Sumatera Utara</option>
                                                <option {{$takmir->province=='Yogyakarta'?'selected':''}}
                                                    value="Yogyakarta">Yogyakarta</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">HP</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $takmir->hp }}" name="hp" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $user->password }}" name="password" type="password"
                                                class="form-control">
                                            @error('password')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    {{-- Mosque Info --}}
                    <div class="row">
                        <div class="col-md-3">
                            @if (strlen($mosque->photo) != 0)
                            <img class="img-thumbnail d-block" src="{{asset('public/storage/mosque/'.$mosque->photo)}}" id='img-mupload' alt="">
                            @else
                            <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-mupload' alt="">
                            @endif
                            <br>
                            <div class="input-group upload">
                                <label class="btn btn-outline-primary shadow" for="mphoto">Upload Foto</label>
                                <input class="form-control d-none" value="{{ $mosque->photo }}"
                                    accept="image/jpeg,image/jpg,image/png," type="file" name="mphoto" id="mphoto">
                                <span class="errorval errorRegis" id="error_photo"></span>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h3 class="card-description">
                                Masjid info
                            </h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input value="{{$mosque->name}}" name="mname" id="mname" type="text"
                                                class="form-control">
                                            @error('name')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label" for="province">
                                                    Provinsi</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="mprovince" id="mprovince">
                                                    <option>  </option>
                                                    <option {{$mosque->province=='Jakarta'?'selected':''}} value="Jakarta">
                                                        Jakarta</option>
                                                    <option {{$mosque->province=='Aceh'?'selected':''}} value="Aceh">Aceh
                                                    </option>
                                                    <option {{$mosque->province=='Bali'?'selected':''}} value="Bali">Bali
                                                    </option>
                                                    <option {{$mosque->province=='Banten'?'selected':''}} value="Banten">
                                                        Banten</option>
                                                    <option {{$mosque->province=='Bengkulu'?'selected':''}}
                                                        value="Bengkulu">Bengkulu</option>
                                                    <option {{$mosque->province=='Gorontalo'?'selected':''}}
                                                        value="Gorontalo">Gorontalo</option>
                                                    <option {{$mosque->province=='Jambi'?'selected':''}} value="Jambi">Jambi
                                                    </option>
                                                    <option {{$mosque->province=='Jawa Barat'?'selected':''}}
                                                        value="Jawa Barat">Jawa Barat</option>
                                                    <option {{$mosque->province=='Jawa Tengah'?'selected':''}}
                                                        value="Jawa Tengah">Jawa Tengah</option>
                                                    <option {{$mosque->province=='Jawa Timur'?'selected':''}}
                                                        value="Jawa Timur">Jawa Timur</option>
                                                    <option {{$mosque->province=='Kalimantan Barat'?'selected':''}}
                                                        value="Kalimantan Barat">Kalimantan Barat</option>
                                                    <option {{$mosque->province=='Kalimantan Selatan'?'selected':''}}
                                                        value="Kalimantan Selatan">Kalimantan Selatan</option>
                                                    <option {{$mosque->province=='Kalimantan Tengah'?'selected':''}}
                                                        value="Kalimantan Tengah">Kalimantan Tengah</option>
                                                    <option {{$mosque->province=='Kalimantan Timur'?'selected':''}}
                                                        value="Kalimantan Timur">Kalimantan Timur</option>
                                                    <option {{$mosque->province=='Kalimantan Utara'?'selected':''}}
                                                        value="Kalimantan Utara">Kalimantan Utara</option>
                                                    <option {{$mosque->province=='Kepulauan Bangka Belitung'?'selected':''}}
                                                        value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung
                                                    </option>
                                                    <option {{$mosque->province=='Kepulauan Riau'?'selected':''}}
                                                        value="Kepulauan Riau">Kepulauan Riau</option>
                                                    <option {{$mosque->province=='Lampung'?'selected':''}} value="Lampung">
                                                        Lampung</option>
                                                    <option {{$mosque->province=='Maluku'?'selected':''}} value="Maluku">
                                                        Maluku</option>
                                                    <option {{$mosque->province=='Maluku Utara'?'selected':''}}
                                                        value="Maluku Utara">Maluku Utara</option>
                                                    <option {{$mosque->province=='Nusa Tenggara Barat'?'selected':''}}
                                                        value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                                    <option {{$mosque->province=='Nusa Tenggara Timur'?'selected':''}}
                                                        value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                                    <option {{$mosque->province=='Papua'?'selected':''}} value="Papua">Papua
                                                    </option>
                                                    <option {{$mosque->province=='Papua Barat'?'selected':''}}
                                                        value="Papua Barat">Papua Barat</option>
                                                    <option {{$mosque->province=='Riau'?'selected':''}} value="Riau">Riau
                                                    </option>
                                                    <option {{$mosque->province=='Sulawesi Barat'?'selected':''}}
                                                        value="Sulawesi Barat">Sulawesi Barat</option>
                                                    <option {{$mosque->province=='Sulawesi Selatan'?'selected':''}}
                                                        value="Sulawesi Selatan">Sulawesi Selatan</option>
                                                    <option {{$mosque->province=='Sulawesi Tengah'?'selected':''}}
                                                        value="Sulawesi Tengah">Sulawesi Tengah</option>
                                                    <option {{$mosque->province=='Sulawesi Tenggara'?'selected':''}}
                                                        value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                                    <option {{$mosque->province=='Sulawesi Utara'?'selected':''}}
                                                        value="Sulawesi Utara">Sulawesi Utara</option>
                                                    <option {{$mosque->province=='Sumatera Barat'?'selected':''}}
                                                        value="Sumatera Barat">Sumatera Barat</option>
                                                    <option {{$mosque->province=='Sumatera Selatan'?'selected':''}}
                                                        value="Sumatera Selatan">Sumatera Selatan</option>
                                                    <option {{$mosque->province=='Sumatera Utara'?'selected':''}}
                                                        value="Sumatera Utara">Sumatera Utara</option>
                                                    <option {{$mosque->province=='Yogyakarta'?'selected':''}}
                                                        value="Yogyakarta">Yogyakarta</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="maddress" id="maddress" cols="30" rows="10">{{ $mosque->address }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Keterangan</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="mdesc" id="mdesc" cols="30" rows="10">{{ $mosque->desc }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <input type="submit" class="btn btn-primary btn-block" value="Edit">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
@endsection