@extends('dashboard.master')
@section('title')
    Edit Admin
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Edit User</h2>
                <form class="form-sample" method="POST" action="{{route('admin.update',$user->uuid)}}"
                    enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('PATCH')}}

                    {{-- Personal Info --}}
                    <div class="row">
                        <div class="col-md-3">
                            @if (strlen($admin->photo) != 0)
                            <img class="img-thumbnail d-block" src="{{asset('public/storage/user/'.$admin->photo)}}" id='img-upload' alt="">
                            @else
                            <img class="img-thumbnail d-block" src="{{asset('img/user.png')}}" id='img-upload' alt="">
                            @endif
                            <br>
                            <div class="input-group upload">
                                <label class="btn btn-outline-primary shadow" for="photo">Upload Foto</label>
                                <input class="form-control d-none" value="{{ $admin->photo }}"
                                    accept="image/jpeg,image/jpg,image/png," type="file" name="photo" id="photo">
                                <span class="errorval errorRegis" id="error_photo"></span>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h3 class="card-description">
                                Personal info
                            </h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input value="{{$user->name}}" name="name" id="name" type="text"
                                                class="form-control">
                                            @error('name')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Email</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $user->email }}" name="email" id="email" type="text"
                                                class="form-control">
                                            @error('email')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                        <div class="col-sm-9">
                                            <select name="gender" class="form-control">
                                                <option {{$admin->gender=='Pria'?'selected':''}} value="Pria">Pria
                                                </option>
                                                <option {{$admin->gender=='Wanita'?'selected':''}} value="Wanita">
                                                    Wanita</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $admin->birth_date }}" style="cursor:pointer" readonly
                                                name="birth_date" id="datepicker" class="form-control"
                                                placeholder="dd-mm-yyyy">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <textarea name="address" type="text"
                                                class="form-control">{{$admin->address}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="province">Provinsi</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="province" id="province">
                                                <option>  </option>
                                                <option {{$admin->province=='Jakarta'?'selected':''}} value="Jakarta">
                                                    Jakarta</option>
                                                <option {{$admin->province=='Aceh'?'selected':''}} value="Aceh">Aceh
                                                </option>
                                                <option {{$admin->province=='Bali'?'selected':''}} value="Bali">Bali
                                                </option>
                                                <option {{$admin->province=='Banten'?'selected':''}} value="Banten">
                                                    Banten</option>
                                                <option {{$admin->province=='Bengkulu'?'selected':''}}
                                                    value="Bengkulu">Bengkulu</option>
                                                <option {{$admin->province=='Gorontalo'?'selected':''}}
                                                    value="Gorontalo">Gorontalo</option>
                                                <option {{$admin->province=='Jambi'?'selected':''}} value="Jambi">Jambi
                                                </option>
                                                <option {{$admin->province=='Jawa Barat'?'selected':''}}
                                                    value="Jawa Barat">Jawa Barat</option>
                                                <option {{$admin->province=='Jawa Tengah'?'selected':''}}
                                                    value="Jawa Tengah">Jawa Tengah</option>
                                                <option {{$admin->province=='Jawa Timur'?'selected':''}}
                                                    value="Jawa Timur">Jawa Timur</option>
                                                <option {{$admin->province=='Kalimantan Barat'?'selected':''}}
                                                    value="Kalimantan Barat">Kalimantan Barat</option>
                                                <option {{$admin->province=='Kalimantan Selatan'?'selected':''}}
                                                    value="Kalimantan Selatan">Kalimantan Selatan</option>
                                                <option {{$admin->province=='Kalimantan Tengah'?'selected':''}}
                                                    value="Kalimantan Tengah">Kalimantan Tengah</option>
                                                <option {{$admin->province=='Kalimantan Timur'?'selected':''}}
                                                    value="Kalimantan Timur">Kalimantan Timur</option>
                                                <option {{$admin->province=='Kalimantan Utara'?'selected':''}}
                                                    value="Kalimantan Utara">Kalimantan Utara</option>
                                                <option {{$admin->province=='Kepulauan Bangka Belitung'?'selected':''}}
                                                    value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung
                                                </option>
                                                <option {{$admin->province=='Kepulauan Riau'?'selected':''}}
                                                    value="Kepulauan Riau">Kepulauan Riau</option>
                                                <option {{$admin->province=='Lampung'?'selected':''}} value="Lampung">
                                                    Lampung</option>
                                                <option {{$admin->province=='Maluku'?'selected':''}} value="Maluku">
                                                    Maluku</option>
                                                <option {{$admin->province=='Maluku Utara'?'selected':''}}
                                                    value="Maluku Utara">Maluku Utara</option>
                                                <option {{$admin->province=='Nusa Tenggara Barat'?'selected':''}}
                                                    value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                                <option {{$admin->province=='Nusa Tenggara Timur'?'selected':''}}
                                                    value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                                <option {{$admin->province=='Papua'?'selected':''}} value="Papua">Papua
                                                </option>
                                                <option {{$admin->province=='Papua Barat'?'selected':''}}
                                                    value="Papua Barat">Papua Barat</option>
                                                <option {{$admin->province=='Riau'?'selected':''}} value="Riau">Riau
                                                </option>
                                                <option {{$admin->province=='Sulawesi Barat'?'selected':''}}
                                                    value="Sulawesi Barat">Sulawesi Barat</option>
                                                <option {{$admin->province=='Sulawesi Selatan'?'selected':''}}
                                                    value="Sulawesi Selatan">Sulawesi Selatan</option>
                                                <option {{$admin->province=='Sulawesi Tengah'?'selected':''}}
                                                    value="Sulawesi Tengah">Sulawesi Tengah</option>
                                                <option {{$admin->province=='Sulawesi Tenggara'?'selected':''}}
                                                    value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                                <option {{$admin->province=='Sulawesi Utara'?'selected':''}}
                                                    value="Sulawesi Utara">Sulawesi Utara</option>
                                                <option {{$admin->province=='Sumatera Barat'?'selected':''}}
                                                    value="Sumatera Barat">Sumatera Barat</option>
                                                <option {{$admin->province=='Sumatera Selatan'?'selected':''}}
                                                    value="Sumatera Selatan">Sumatera Selatan</option>
                                                <option {{$admin->province=='Sumatera Utara'?'selected':''}}
                                                    value="Sumatera Utara">Sumatera Utara</option>
                                                <option {{$admin->province=='Yogyakarta'?'selected':''}}
                                                    value="Yogyakarta">Yogyakarta</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">HP</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $admin->hp }}" name="hp" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input value="{{ $user->password }}" name="password" type="password"
                                                class="form-control">
                                            @error('password')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary btn-block" value="Edit">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
@endsection