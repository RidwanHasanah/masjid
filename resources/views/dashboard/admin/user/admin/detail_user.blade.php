@extends('dashboard.master')
@section('title')
Info Admin
@endsection
@section('content')
<div class="row">
  @include('layouts.patrials.alerts')
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="card-title">
          <h5 class="">Info Admin</h5>
        </div>
        <div class="">
          <div class="row">
            <div class="col-md-3 col-lg-3 " align="center">
              @if (strlen($admin->photo) != 0)
              <img alt="User Pic" src="{{asset('public/storage/user/'.$admin->photo)}}" class="img-circle img-responsive img-thumbnail">         
              @else
              <img alt="User Pic" src="{{asset('img/user.png')}}" class="img-circle img-responsive img-thumbnail"> 
              @endif
              
            </div>
            <div class="table-responsive col-md-9 col-lg-9 ">
              <table class="table table-striped table-hover">
                <tbody>
                  <tr>
                    <td><b>Nama </b></td>
                    <td>{{$user->name}}</td>
                  </tr>
                  <tr>
                    <td><b>Tempat, Tanggal Lahir</b></td>
                    <td>{{$admin->birth_date}} , {{$admin->birth_place}}</td>
                  </tr>
                  <tr>
                    <td><b>Jenis Kelamin</b></td>
                    <td>{{$admin->gender}}</td>
                  </tr>
                  <tr>
                    <td><b>Provinsi</b></td>
                    <td>{{$admin->province}}</td>
                  </tr>
                  <tr>
                    <td><b>Alamat</b></td>
                    <td>{{$admin->address}}</td>
                  </tr>
                  <tr>
                    <td><b>Email</b></td>
                    <td><a href="{{$user->email}}">{{$user->email}}</a></td>
                  </tr>
                  <tr>
                    <td><b>Nomor HandPhone</b></td>
                    <td>{{$admin->hp}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection