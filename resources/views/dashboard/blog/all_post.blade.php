@extends('dashboard.master')
@section('title')
Semua Tulisan
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        @include('layouts.patrials.alerts')
        <div class="row">
          <div class="col-md-6">
            <h4 class="card-title"> Semua Tulisan </h4>
          </div>
          <div class="col-md-6"><a class="btn btn-success float-right" href="{{route('blog.create')}}">Tambah
              Tulisan</a></div>
        </div>
        <div class="center-pos" id="loadingDiv">
          <img class="loading" src="{{asset('img/loading.gif')}}">
        </div>
        <div class="table-responsive">
          <table id="table-admin" class="table table-striped table-hover">
            <thead>
              <tr>
                <th>Title</th>
                <th>Kategori</th>
                <th>Tanggal</th>
                <th></th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  var ajaxApi = '{{route("api.blog")}}'
    var table = $('#table-admin').DataTable({
        order: [[1,'desc']],
        processing: true,
        serverSide: true,
        ajax : ajaxApi,
        columns : [
            {data: 'title', name: 'title'},
            {data: 'kategori', name: 'kategori'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action',ordertable: false, searchable:false},
        ]

    })
    

    // Delete Data
    function deletePost(id){
        // alert('bismillah');
        
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        Swal.fire({
        title: 'Kamu yakin?',
        text: "Artikel tidak akan bisa kembali!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus saja!',
        cancelButtonText: 'Tidak jadi'
      }).then((result) => {
        if (result.value) {
          $.ajax({
                  url: "{{ url('blog') }}" + '/' + id,
                  type: "DELETE",
                  data: {'_method' : 'DELETE', '_token' : csrf_token},
                  success: function(data){
                    table.ajax.reload();
                    Swal.fire({
                        title: 'Terhapus!',
                        text: 'Artikel Sudah di Hapus',
                        type: 'success',
                        timer: 1500

                    })
                  },
                  error: function(){
                    Swal.fire({
                      title: 'oops',
                      text: "Adaya Salah",
                      type: 'error'
                    })
                  }
                })
        }
      })
    }
</script>
@endsection