@extends('dashboard.master')
@section('title')
    {{$blog->title}}
@endsection
@section('content')
<div class="container-fluid p-5 pt-max bg-grad" height="50%">
    <div class="row">
        @include('layouts.patrials.alerts')
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-title text-center">
                        <br>
                        <h1 class="card-title text-main">
                            <b>{{$blog->title}}</b>
                        </h1> 
                        
                        </div>

                        <div class="row text-main justify-content-center">
                        @if (strlen($blog->photo) != 0)
                            <img width="50%" src="{{ asset('public/storage/blog/'.$blog->photo)}}" alt="">
                        @else
                            <img width="50%" class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                        @endif
                        </div>

                        <div class="card-body">
                            <p class="card-text">{!! $blog->content !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection