@extends('dashboard.master')
@section('title')
Edit Tulisan
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Edit Tulisan</h2>
                <form class="form-sample" method="POST" action="{{route('blog.update',$blog->uuid)}}"
                    enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('PATCH')}}

                    {{-- Personal Info --}}
                    <div class="row">
                        <div class="col-md-3">
                            @if (strlen($blog->photo) != 0)
                            <img class="img-thumbnail d-block" src="{{asset('public/storage/blog/'.$blog->photo)}}"
                                id='img-upload' alt="">
                            @else
                            <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload'
                                alt="">
                            @endif
                            <br>
                            <div class="input-group upload">
                                <label class="btn btn-outline-primary shadow" for="photo">Upload Foto</label>
                                <input class="form-control d-none" value="{{ $blog->photo }}"
                                    accept="image/jpeg,image/jpg,image/png," type="file" name="photo" id="photo">
                                <span class="errorval errorRegis" id="error_photo"></span>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="kategori">Kategori</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="kategori" id="kategori">
                                                <option>  </option>
                                                <option {{$blog->kategori=='blog'?'selected':''}} value="blog">
                                                    Blog</option>
                                                <option {{$blog->kategori=='jadwal pengajian'?'selected':''}} value="jadwal pengajian">
                                                    Jadwal Pengajian</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Title</label>
                                        <div class="col-sm-10">
                                            <input value="{{$blog->title}}" name="title" id="title" type="text"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Tulisan</label>
                                        <div class="col-sm-10">
                                            <textarea name="content" id="content" cols="10" rows="20"
                                                class="form-control wysiwyg">{{ $blog->content }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary btn-block" value="Edit">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection