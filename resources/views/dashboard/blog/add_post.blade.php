@extends('dashboard.master')
@section('title')
Tambah Post
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Tambah Post</h2>
                <form class="form-sample" method="POST" action="{{route('blog.store')}}"
                    enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('POST')}}

                    {{-- Personal Info --}}
                    <div class="row">
                        <div class="col-md-3">
                            <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                            <br>
                            <div class="input-group upload">
                                <label class="btn btn-outline-primary shadow" for="photo">Upload Foto</label>
                                <input class="form-control d-none" value="{{ old('photo') }}"
                                    accept="image/jpeg,image/jpg,image/png," type="file" name="photo" id="photo">
                                <span class="errorval errorRegis" id="error_photo"></span>
                            </div>
                        </div>

                        
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="kategori">Kategori</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="kategori" id="kategori">
                                                <option>  </option>
                                                <option value="blog">
                                                    Blog</option>
                                                <option value="jadwal pengajian">
                                                    Jadwal Pengajian</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Judul</label>
                                        <div class="col-sm-10">
                                            <input value="{{ old('title') }}" name="title" id="title" type="text"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Tulisan</label>
                                        <div class="col-sm-10">
                                            <textarea name="content" id="content" cols="10" rows="20" class="form-control wysiwyg">{{ old('content') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary btn-block" value="Simpan">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
@endsection