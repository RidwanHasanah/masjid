<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      {{-- <li class="nav-item nav-profile">
        <div class="nav-link">
          <div class="user-wrapper">
            <div class="profile-image">
              <img src="{{asset('img/face1.jpg')}}" alt="profile image">
            </div>
            <div class="text-wrapper">
              <p class="profile-name">{{Auth::user()->name}}</p>
              <div>
                <small class="designation text-muted"> </small>
                <span class="status-indicator online"></span>
              </div>
            </div>
          </div>
          <button class="btn btn-success btn-block">New Project
            <i class="mdi mdi-plus"></i>
          </button>
        </div>
      </li> --}}
      @if(!Auth::user()->hasRole('master') && !Auth::user()->hasRole('admin'))
      <li class="nav-item">
        <a class="nav-link" target="_blank" href="{{route('web', [Auth::user()->name, Auth::user()->takmir()->first()->mosque()->first()->slug] )}}">
          <i class="menu-icon fa fa-globe"></i>
          <span class="menu-title">Web Profile</span>
        </a>
      </li>
      @endif

      <li class="nav-item">
        <a class="nav-link" href="{{route('home')}}">
          <i class="menu-icon mdi mdi-television"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#artikel" aria-expanded="false" aria-controls="artikel">
          <i class="menu-icon mdi mdi-lead-pencil"></i>
          <span class="menu-title">Artikel</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="artikel">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{route('blog.index')}}">Semua Artikel</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('blog.create')}}">Tambah Artikel</a>
            </li>
          </ul>
        </div>
      </li>
      
      @if (Auth::user()->hasRole('admin'))
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
          <i class="menu-icon fa fa-users"></i>
          <span class="menu-title">Users</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{route('takmir.index')}}">Semua Takmir</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('takmir.create')}}">Tambah Takmir</a>
            </li>
          </ul>
          @if (Auth::user()->hasRole('master'))
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{route('admin.index')}}">Semua Admin</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('admin.create')}}">Tambah Admin</a>
            </li>
          </ul>
        </div>
        @endif
      </li>
      @endif
      
      @if (!Auth::user()->hasRole('master') && !Auth::user()->hasRole('admin'))
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#jamaah" aria-expanded="false" aria-controls="jamaah">
          <i class="menu-icon fa fa-users"></i>
          <span class="menu-title">Jamaah</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="jamaah">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{route('jamaah.index')}}">Semua Jamaah</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('jamaah.create')}}">Tambah Jamaah</a>
            </li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#Gallery" aria-expanded="false" aria-controls="Gallery">
          <i class="menu-icon fa fa-image"></i>
          <span class="menu-title">Gallery</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="Gallery">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{route('gallery.index')}}">Semua Album</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('gallery.create')}}">Tambah Gallery</a>
            </li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#Kas" aria-expanded="false" aria-controls="Kas">
          <i class="menu-icon fa fa-money"></i>
          <span class="menu-title">Kas</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="Kas">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('debit.index')}}">Kas Masuk</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('credit.index')}}">Kas Keluar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('debit.create')}}">Tambah Kas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('credit.create')}}">Tambah Pengeluaran</a>
            </li>
          </ul>
        </div>
      </li>
      
      <li class="nav-item">
          <a class="nav-link dropdown-item mt-2" href="{{route('organisasi')}}">
            <i class="menu-icon fa fa-sitemap""></i>
            <span class="menu-title">Organisasi</span>
          </a>
      </li>
      
      <li class="nav-item">
          <a class="nav-link dropdown-item mt-2" href="{{route('profile')}}">
            <i class="menu-icon mdi mdi-account""></i>
            <span class="menu-title">Manage Accounts</span>
          </a>
        @endif
      </li>

      <li class="nav-item logout">
        <a class="nav-link dropdown-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
          <i class="menu-icon fa fa-sign-out"></i>
          <span class="menu-title">Logout</span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
      </li>
             
    </ul>
    
    <!-- <div class="collapse"> -->
      <!-- <ul class="nav flex-column sub-menu">
      </ul>

      <ul class="nav flex-column sub-menu">
      </ul> -->
    <!-- </div> -->
      
    <!-- <ul class="nav flex-column sub-menu">
    </ul> -->
  </nav>
  <!-- partial -->