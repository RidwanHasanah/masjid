@extends('dashboard.master')
@section('title')
Manajemen Masjid
@endsection
@section('content')

    <div class="row">
      @if (Auth::user()->hasRole('master') || Auth::user()->hasRole('admin'))
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-cube text-danger icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Masjid</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{$masjid}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-6 mb-0">
              <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> Masjid terdaftar
            </p>
          </div>
        </div>
      </div>

      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-account-location text-info icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Takmir</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{$takmir}}</h3>
                </div>
              </div>
            </div>
            <p class="text-muted mt-6 mb-0">
              <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Takmir terdaftar
            </p>
          </div>
        </div>
      </div>
      
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="fa fa-users text-info icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Jamah Indonesia</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0">{{$Jamaah}}</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

        @if(Auth::user()->hasRole('master'))
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    @include('layouts.patrials.alerts')
                    <div class="row">
                        <div class="col-md-6"><h4 class="card-title"> Semua Kritk & Saran </h4></div>
                    </div>
                    <div class="center-pos" id="loadingDiv">
                            <img class="loading" src="{{asset('img/loading.gif')}}">
                    </div>
                    <div class="table-responsive">
                        <table id="table-saran"  class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Subject</th>
                                    <th>Pesan</th>
                                    <th>di Kirim</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
        @endif


      @elseif (Auth::user()->hasRole('takmir'))
      

      
      <!-- Kas Perminggu -->

      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-calendar text-primary icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Kas Masuk Mingguan</p>
                
                <div class="fluid-container">
                  <h4 class="font-weight-medium text-right mb-0">Rp. {{number_format($week, 0, '', '.')}}</h4>
                </div>

              </div>
            </div>
            <p class="mb-0 text-right">
              {{terbilang($week)}}
            </p>
            <p class="text-muted mt-4 mb-0">
              <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Diperbarui Setiap Hari Senin
            </p>
          </div>
        </div>
      </div>
      
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-calendar text-primary icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Kas Keluar Mingguan</p>
                <div class="fluid-container">
                  <h4 class="font-weight-medium text-right mb-0">Rp. {{number_format($weekcredit, 0, '', '.')}}</h4>
                </div>
              </div>
            </div>
            <p class="mb-0 text-right">
              {{terbilang($weekcredit)}}
            </p>
            <p class="text-muted mt-4 mb-0">
              <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Diperbarui Setiap Hari Senin
            </p>
          </div>
        </div>
      </div>

      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-calendar text-primary icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Total Kas Minggu ini</p>
                <div class="fluid-container">
                  <h4 class="font-weight-medium text-right mb-0">Rp. {{number_format($week - $weekcredit, 0, '', '.')}}</h4>
                </div>
              </div>
            </div>
            <p class="mb-0 text-right">
              {{terbilang($week - $weekcredit)}}
            </p>
            <p class="text-muted mt-4 mb-0">
              <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Total Diperbarui Setiap Hari Senin
            </p>
          </div>
        </div>
      </div>


      <!-- Kas Per Bulan -->
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-calendar text-warning icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Kas Masuk Bulan ini</p>
                <div class="fluid-container">
                  <h4 class="font-weight-medium text-right mb-0">Rp. {{number_format($month, 0, '', '.')}}</h4>
                </div>
              </div>
            </div>
            <p class="mb-0 text-right">
              {{terbilang($month)}}
            </p>
            <p class="text-muted mt-4 mb-0">
              <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Kas Masuk Bulan ini
            </p>
          </div>
        </div>
      </div>
      
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-calendar text-warning icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Kas Keluar Bulan ini</p>
                <div class="fluid-container">
                  <h4 class="font-weight-medium text-right mb-0">Rp. {{number_format($monthcredit, 0, '', '.')}}</h4>
                </div>
              </div>
            </div>
            <p class="mb-0 text-right">
              {{terbilang($monthcredit)}}
            </p>
            <p class="text-muted mt-4 mb-0">
              <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Kas Keluar Bulan ini
            </p>
          </div>
        </div>
      </div>
      
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-calendar text-warning icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Total Kas Bulan ini</p>
                <div class="fluid-container">
                  <h4 class="font-weight-medium text-right mb-0">Rp. {{number_format($month - $monthcredit, 0, '', '.')}}</h4>
                </div>
              </div>
            </div>
            <p class="mb-0 text-right">
              {{terbilang($month - $monthcredit)}}
            </p>
            <p class="text-muted mt-4 mb-0">
              <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> Total Kas Bulan ini
            </p>
          </div>
        </div>
      </div>  

      
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-poll-box text-success icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Kas Masjid</p>
                <div class="fluid-container">
                  <h4 class="font-weight-medium text-right mb-0">Rp. {{number_format($debit-$credit, 0, '', '.')}}</h4>
                </div>
              </div>
            </div>
            <p class="mb-0 text-right">
              {{terbilang($debit - $credit)}}
            </p>
            <p class="text-muted mt-6 mb-0">
              <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> Product-wise sales
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="clearfix">
              <div class="float-left">
                <i class="mdi mdi-account-location text-info icon-lg"></i>
              </div>
              <div class="float-right">
                <p class="mb-0 text-right">Jamaah</p>
                <div class="fluid-container">
                  <h3 class="font-weight-medium text-right mb-0"> {{ $jamaah }} </h3>
                </div>
              </div>
            </div>
            <p class="mb-0 text-right">
              {{terbilang($jamaah)}}
            </p>
            <p class="text-muted mt-6 mb-0">
              <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Jamaah dimasjid ini
            </p>
          </div>
        </div>
      </div>

      <a href="{{route ('saran.create')}}" class="float" target="_blank">
        <div class="kritik">
          <i class="fa fa-lightbulb-o my-float  text-black"> 
            </i>
              <div class="costum-text text-black">
                Kritik & Saran            
              </div>
        </div>
      </a>
      @endif
    </div>
   
@endsection
@section('js')
    <script type="text/javascript">
    
    var ajaxApi = '{{route("api.saran")}}'
    var table = $('#table-saran').DataTable({
        order: [[1,'desc']],
        processing: true,
        serverSide: true,
        ajax : ajaxApi,
        columns : [
            {data: 'subject', name: 'subject'},
            {data: 'suggest', name: 'suggest'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action',ordertable: false, searchable:false},
        ]

    })

    

    // Delete Data
    function deletesaran(id){
        // alert(id);
        
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        Swal.fire({
          title: 'Kamu yakin ?',
          text: "User yang di hapus tidak akan kembali!",
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Ya, Hapus ini!'
        }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "{{ url('saran') }}" + '/' + id,
            type: "DELETE",
            data: {'_method' : 'DELETE', '_token' : csrf_token},
            success: function(data){
              table.ajax.reload();

              console.log(data)
              Swal.fire({
                       title: 'Terhapus!',
                        text: 'saran Sudah di Hapus',
                        type: 'success',
                        timer: 1500
              })
            },
            error: function(data){
              Swal.fire({
                      title: 'oops',
                      text: "Adaya Salah",
                      type: 'error',
                      timer: 1500
                    })

              console.log(data)
            }
          })
        }
      })
    }
    </script>
@endsection