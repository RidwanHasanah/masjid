@extends('dashboard.master')
@section('title')
Tutorial GMaps
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title text-center">
                    <b>
                        Cara Manggunakan Google Maps
                    </b>
                </h2>
                <div class="card-text">
                    <p> <b> 1. Buka Google Maps</b></p>
                    
                    <p> <b> 2. Cari alamat di form telusuri</b></p>
                    
                    <div class="col-6 scroll">
                        <img src="{{asset('img/1.png')}}" alt="">
                    </div><br>

                    <p> <b> 3. Klik bagikan</b></p>
                    
                    <div class="col-6 scroll">
                        <img src="{{asset('img/2.png')}}" alt="">
                    </div><br>
                    
                    <p><b> 4. Pilih sematkan peta & salin link-nya</b></p>
                    
                    <div class="col-6 scroll">
                        <img src="{{asset('img/3.png')}}" alt="">
                    </div><br>
                    
                    <p><b> 5. Kemudian kembali ke form edit profile</b></p>
                    
                    <div class="col-6 scroll">
                        <img src="{{asset('img/4.png')}}" alt="">
                    </div><br>
                    
                    <p><b> 6. Tempel link yang sudah tersalin ke form</b></p>
                    
                    <div class="col-6 scroll">
                        <img src="{{asset('img/5.png')}}" alt="">
                    </div><br>
                    
                    <p><b> 7. Hapus kalimat (&lt;iframe src=")</iframe> </b></p>

                    <div class="col-6 scroll">
                        <img src="{{asset('img/6.png')}}" alt="">
                    </div><br>

                    <p><b> 8. Hapus juga kalimat dimulai dari tanda petik sebelum width (" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""&gt;&lt;/iframe&gt;)</iframe> </b></p>

                    <div class="col-6 scroll">
                        <img src="{{asset('img/7.png')}}" alt="">
                    </div><br>
                    
                    <p><b> 9. Kemudian klik tombol jika sudah selesai merubah profile klik tombol edit di bawah</b></p>

                    <div class="col-6 scroll">
                        <img src="{{asset('img/8.png')}}" alt="">
                    </div><br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection