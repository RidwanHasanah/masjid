
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Manajemen Masjid</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Manajemen Masjid https://masjidindonesia.org" />
	<meta name="keywords" content="masjid, Manajemen Masjid, masjid management "/>
	<meta name="author" content="https://masjidindonesia.org" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />
	<meta name="google-site-verification" content="QmbK8FaDpAPCOV7rE9OqnAubogLQiEZhHOxJNy2kiMA" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{asset('frontpage/main/css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{asset('frontpage/main/css/icomoon.css')}}">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="{{asset('frontpage/main/css/simple-line-icons.css')}}">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{asset('frontpage/main/css/magnific-popup.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{asset('frontpage/main/css/bootstrap.css')}}">

	<!-- 
	Default Theme Style 
	You can change the style.css (default color purple) to one of these styles
	
	1. pink.css
	2. blue.css
	3. turquoise.css
	4. orange.css
	5. lightblue.css
	6. brown.css
	7. green.css

	-->
	<link rel="stylesheet" href="{{asset('frontpage/main/css/style.css')}}">


	<!-- Modernizr JS -->
	<script src="{{asset('frontpage//main/js/modernizr-2.6.2.min.js')}}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
        @yield('content')
	<!-- jQuery -->
	<script src="{{asset('frontpage/main/js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{asset('frontpage/main/js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{asset('frontpage/main/js/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{asset('frontpage/main/js/jquery.waypoints.min.js')}}"></script>
	<!-- Stellar Parallax -->
	<script src="{{asset('frontpage/main/js/jquery.stellar.min.js')}}"></script>
	<!-- Counter -->
	<script src="{{asset('frontpage/main/js/jquery.countTo.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{asset('frontpage/main/js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{asset('frontpage/main/js/magnific-popup-options.js')}}"></script>

	<!-- For demo purposes only styleswitcher ( You may delete this anytime ) -->
	<script src="{{asset('frontpage/main/js/jquery.style.switcher.js')}}"></script>
	<script>
		$(function(){
			$('#colour-variations ul').styleSwitcher({
				defaultThemeId: 'theme-switch',
				hasPreview: false,
				cookie: {
		          	expires: 30,
		          	isManagingLoad: true
		      	}
			});	
			$('.option-toggle').click(function() {
				$('#colour-variations').toggleClass('sleep');
			});
		});

		function Berita()
		{
			window.location.replace("{{ url('news')}}")
		}

		function Home()
		{
			window.location.replace("{{route('home.index')}}")
		}
	</script>
	<!-- End demo purposes only -->

	<!-- Main JS (Do not remove) -->
	<script src="{{asset('frontpage/main/js/main.js')}}"></script>

	</body>
</html>

