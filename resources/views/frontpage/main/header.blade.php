<header role="banner" id="fh5co-header">
    <div class="container">
        <!-- <div class="row"> -->
        <nav class="navbar-default">
        <div class="navbar-header">
            <!-- Mobile Toggle Menu Button -->
            <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
          <ul class="nav navbar-nav navbar-right">
            <li ><a href="fh5co-home" data-nav-section="home"><span>Beranda</span></a></li>
            {{-- <li><a href="#" data-nav-section="work"><span>Work</span></a></li> --}}
            {{-- <li><a href="#" data-nav-section="testimonials"><span>Testimonials</span></a></li> --}}
            <li><a href="#fh5co-services" data-nav-section="services"><span>Layanan</span></a></li>
            <li><a href="#fh5co-about" data-nav-section="about"><span>Tentang</span></a></li>
            <li><a href="#fh5co-contact" data-nav-section="contact"><span>Kontak</span></a></li>
            <li><a onclick="Berita()" ><span>Berita</span></a></li>
          </ul>
        </div>
        </nav>
      <!-- </div> -->
  </div>
</header>