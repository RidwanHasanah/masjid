<div class="container-fluid p-5 pt-max bg-grad" height="50%">
    <h1 class="text-white">Blog</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="row text-main justify-content-center">
                @foreach ($blog as $news)
                <div class="col-md-3">
                    <div class="card">
                        @if (strlen($news->photo) != 0)
                            <img src="{{ asset('public/storage/blog/'.$news->photo) }}" class="card-img-top" alt="...">
                        @else
                            <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                        @endif
                        <div class="card-body">
                            <h3 class="card-title text-main"> <a href="{{route('news.show',$news->slug)}}">{{ $news->title }}</a></h3>
                            <p class="card-text"> {!! substr($news->content,0,50) !!} ...</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>