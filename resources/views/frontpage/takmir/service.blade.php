<section id="fh5co-services" data-section="services">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="to-animate fadeInUp animated text-white">Jadwal Pengajian</h2>
                @if($data['blog1'] === 0)
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 subtext to-animate fadeInUp animated">
                        <h4 class="text-white">
                            <span>
                                (atas izin Allah subhanahu wa ta'ala)
                            </span>
                            <br> 
                            Tidak ada Jadwal Kajian di Minggu ini 
                        </h4 class="text-white">
                    </div>
                </div>
            </div>
            
                @else

                <div class="row">
                    <div class="col-md-8 col-md-offset-2 subtext to-animate fadeInUp animated">
                        <h4 class="text-white">
                            Jadwal Pengajian Di Masjid {{$data['mosque']->name}}
                            <br> 
                            <span>
                                ( Insyaallah )
                            </span>
                        </h4 class="text-white">
                        <span>

                            <p>( Di ubah setiap hari senin )</p>
                        </span>
                    </div>
                </div>

                <div class="row row-bottom-padded-sm kartu">
                    @foreach($data['blog'] as $jadwal)
                    <div class="col-md-3 col-sm-6 col-xxs-12 tabel ">
                        <a href="{{ asset('public/storage/blog/'.$jadwal->photo) }}" class="fh5co-services image-popup to-animate fadeInUp animated">
                            @if (strlen($jadwal->photo) != 0)
                                <img class="img-thumbnail d-block img-responsive" src="{{ asset('public/storage/blog/'.$jadwal->photo) }}" id='img-upload' alt="Image">
                            @else
                                <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                            @endif
                        </a>
                            
                        <div class="fh5co-text jadwal">
                            <h2 class="judul">
                                <a target="_blank" href="{{route('jadwal.show',$jadwal->slug)}}" class="fh5co-services">
                                    {{$jadwal->title}}
                                </a>
                            </h2>
                            <div class="ribbon-wrapper-green">
                                <div class="ribbon-green">Jadwal</div>
                            </div>
                            <span class="clearfix"></span>
                            <h4>

                                <span class="konten">{!! substr($jadwal->content,0,50) !!} <br> ...</span>
                            </h4>
                        </div>
                    </div>                    
                    @endforeach
                </div>
                @endif
				

        </div>
    </div>
</section>
