@extends('frontpage.takmir.gallery.master')
@include('frontpage.takmir.gallery.header')
<div class="container-fluid p-5 pt-max bg-grad" height="50%">
    <div class="card bg-grad  class-card">
        <div class="card-body judul">
        <div class="card-header text-center">
                <h2 class="text-center text-white">Album {{$data['album']->name}}</h2> 
            </div>  
            <hr>
        </div>
        <div class="row row-bottom-padded-sm album">
            <!-- <div class="col-md-12"> -->
                @foreach ($data['gallery'] as $gallery)
                <div class="col-md-4 col-sm-6 col-xxs-12">
                    <div class="card foto">
                        <div class="card-img">
                            <a href="{{ asset('storage/gallery/'.$gallery->photo) }}" class="fh5co-services image-popup to-animate fadeInUp animated">
                                @if (strlen($gallery->photo) != 0)
                                <img  src="{{ asset('storage/gallery/'.$gallery->photo) }}" class="img-thumbnail d-block img-responsive" alt="Image" id="img-upload">
                                @else
                                <img  class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            <!-- </div> -->
        </div>
    </div>
</div>
@include('frontpage.main.footer')