@extends('frontpage.main.master')
@include('frontpage.main.blog.header')
<div class="container-fluid p-5 pt-max bg-grad" height="50%">
        <div class="row">
            <div class="col-md-12">
                <div class="row text-main justify-content-center">
                    <h1 class="text-white" >{{$blog->title}}</h1>
                    <hr>
                    <div>
                        
                    @if (strlen($blog->photo) != 0)
                        <img width="50%" src="{{ asset('public/storage/blog/'.$blog->photo) }}" class="card-img-top" alt="...">
                    @else
                        <img width="50%" class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                    @endif
                    </div>
                    <div class="text-white">{!! $blog->content !!}</div>
                </div>
            </div>
        </div>
    </div>
@include('frontpage.main.footer')