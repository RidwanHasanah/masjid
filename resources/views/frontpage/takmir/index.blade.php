@extends('frontpage.takmir.master')
@section('content')
@include('frontpage.takmir.header')
@include('frontpage.takmir.home')
@include('frontpage.takmir.intro')
{{-- @include('frontpage.takmir.work') --}}
{{-- @include('frontpage.takmir.testimonial') --}}
@include('frontpage.takmir.service')    
@include('frontpage.takmir.about')
@include('frontpage.takmir.gallery')
{{-- @include('frontpage.takmir.counter') --}}
@include('frontpage.takmir.contact')
@include('frontpage.takmir.footer')
@endsection