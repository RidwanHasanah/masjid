<section id="fh5co-intro" style="padding-bottom:0;">
    <div class="container">
        <div class="row row-bottom-padded-lg cc_cursor">
            <div class="fh5co-block to-animate" style="background-image: url(https://images.unsplash.com/photo-1541432901042-2d8bd64b4a9b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=719&q=80);">
                <div class="overlay-darker"></div>
                <div class="overlay"></div>
                <div class="fh5co-text">
                    <i class="fh5co-intro-icon icon-book"></i>
                    <h2>Kas Masuk</h2>
                    <h4>
                        <b>Rp. {{number_format($data['debit'], 0, '', '.')}}</b> <br>
                    </h4>
                    <!-- <p>
                        <b> {{terbilang($data['debit'])}} </b> <br>

                    </p> -->
                    <p>
                        "Apa sahaja yang kamu infakkan pada jalan Allah nescaya akan dibalas dengan cukup kepadamu dan kamu tidak akan dianiaya". <br> (QS. Al-Anfaal : 60). 
                    </p>
                    {{-- <p><a href="#" class="btn btn-primary">Get In Touch</a></p> --}}
                </div>
            </div>
            <div class="fh5co-block to-animate" style="background-image: url(https://images.unsplash.com/photo-1519834089823-08a494ba5a12?ixlib=rb-1.2.1&auto=format&fit=crop&w=668&q=80);">
                <div class="overlay-darker"></div>
                <div class="overlay"></div>
                <div class="fh5co-text">
                    <i class="fh5co-intro-icon icon-dashboard"></i>
                    <h2>Kas Keluar</h2>
                    <h4>
                        <b>Rp. {{number_format($data['credit'], 0, '', '.')}}</b> <br>
                    </h4>
                    <!-- <p>
                        <b> {{terbilang($data['credit'])}} </b> <br>
                        
                    </p> -->
                    <p> 
                        “Dan belanjakanlah (harta benda kalian) di jalan Allah, dan janganlah kalian menjatuhkan diri kalian sendiri ke dalam kebinasaan”. <br> (QS. Al Baqarah : 195).
                    </p>
                    {{-- <p><a href="#" class="btn btn-primary">Click Me</a></p> --}}
                </div>
            </div>
            <div class="fh5co-block to-animate" style="background-image: url(https://images.unsplash.com/photo-1531804308561-b6438d25a810?ixlib=rb-1.2.1&auto=format&fit=crop&w=352&q=80);">
                <div class="overlay-darker"></div>
                <div class="overlay"></div>
                <div class="fh5co-text">
                    <i class="fh5co-intro-icon icon-heart"></i>
                    <h2>Total Kas Masjid</h2>
                    <h4>
                        Rp. {{number_format($data['debit']-$data['credit'], 0, '', '.')}} <br>
                    </h4>
                    <!-- <p>
                        <b>{{terbilang($data['debit']-$data['credit'])}}</b> <br>
                        
                    </p> -->
                    <p>
                        "Dan mereka memberi makanan yang disukainya kepada orang miskin, anak yatim dan orang yang ditawan". <br>(QS. Al-Insan : 8).
                    </p>
                    {{-- <p><a href="#" class="btn btn-primary">Why Us?</a></p> --}}
                </div>
            </div>
        </div>
        {{-- <div class="row watch-video text-center to-animate">
            <span>Watch the video</span>

            <a href="https://vimeo.com/channels/staffpicks/93951774" class="popup-vimeo btn-video"><i class="icon-play2"></i></a>
        </div> --}}
    </div>
</section>