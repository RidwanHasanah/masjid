<section id="fh5co-services" data-section="gallery">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="to-animate fadeInUp animated text-white">Gallery</h2>
                @if(strlen($data['album']) === 0)
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 subtext to-animate fadeInUp animated">
                        <h4 class="text-white">
                            <span>
                                (atas izin Allah subhanahu wa ta'ala)
                            </span>
                            <br> 
                            Belum Ada Gallery Foto
                        </h4 class="text-white">
                    </div>
                </div>
            </div>
            
                @else

                <div class="row">
                    <div class="col-md-8 col-md-offset-2 subtext to-animate fadeInUp animated">
                        <h4 class="text-white">
                            Gallery Foto Di Masjid {{$data['mosque']->name}}
                            <br> 
                            <!-- <span>
                                ( Insyaallah )
                            </span> -->
                        </h4>
                        
                    </div>
                </div>

                <div class="row row-bottom-padded-sm kartu">
                    @foreach($data['album'] as $album)
                    <div class="col-md-3 col-sm-6 col-xxs-12 tabel ">
                        <!-- <a href="{{ asset('storage/gallery/'.$album->gallery()->first()->photo) }}" class="fh5co-services image-popup to-animate fadeInUp animated"> -->
                            <!-- </a> -->

                        <a target="_blank" href="{{ route('album.show', $album->uuid) }}" >
                            @if (strlen($album->gallery()->first()->photo) != 0)
                                <img class="img-thumbnail d-block img-responsive" src="{{ asset('storage/gallery/'.$album->gallery()->first()->photo) }}" id='img-upload' alt="Avatar">
                                @else
                                <img class="img-thumbnail d-block" src="{{asset('img/noimage.png')}}" id='img-upload' alt="">
                            @endif
                                
                            <div class="fh5co-text jadwal">
                                <h2 class="judul justify-content-center">
                                        {{$album->name}}
                                </h2>
                                <div class="ribbon-wrapper-green">
                                    <div class="ribbon-green">Album</div>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                        </a>

                    </div>                    
                    @endforeach
                </div>
                @endif
				

        </div>
    </div>
</section>
