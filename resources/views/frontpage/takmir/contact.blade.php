<section id="fh5co-contact" data-section="contact">
    <div class="container">
        {{-- <div class="row">
            <div class="col-md-12 section-heading text-center">
                <h2 class="to-animate">Get In Touch</h2>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 subtext to-animate">
                        <h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                            there live the blind texts.</h3>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="row row-bottom-padded-md justify-content-center">
            <div class="col-md-12 to-animate">
                <h3>Contact Info</h3>
                <ul class="fh5co-contact-info">
                    <li class="fh5co-contact-address ">
                        <i class="icon-home"></i>
                        {{$data['mosque']->address}}, {{$data['mosque']->province}}Indonesia
                    </li>
                    <li><i class="icon-phone"></i> {{$data['takmir']->hp}} </li>
                    <li><i class="icon-envelope"></i>{{$data['user']->email}}</li>
                </ul>
            </div>

            {{-- <div class="col-md-6 to-animate">
                <h3>Contact Form</h3>
                <div class="form-group ">
                    <label for="name" class="sr-only">Name</label>
                    <input id="name" class="form-control" placeholder="Name" type="text">
                </div>
                <div class="form-group ">
                    <label for="email" class="sr-only">Email</label>
                    <input id="email" class="form-control" placeholder="Email" type="email">
                </div>
                <div class="form-group ">
                    <label for="phone" class="sr-only">Phone</label>
                    <input id="phone" class="form-control" placeholder="Phone" type="text">
                </div>
                <div class="form-group ">
                    <label for="message" class="sr-only">Message</label>
                    <textarea name="" id="message" cols="30" rows="5" class="form-control"
                        placeholder="Message"></textarea>
                </div>
                <div class="form-group ">
                    <input class="btn btn-primary btn-lg" value="Send Message" type="submit">
                </div>
            </div> --}}
        </div>

    </div>
    </div>
    <iframe src="{{$data['mosque']->gmaps}}" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    {{-- <div id="map" class="to-animate"></div> --}}
</section>