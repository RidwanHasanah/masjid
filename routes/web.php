<?php

Route::get('/', function () {
    return view('frontpage.main.index');
})->name('home.index');
Route::get('/news','FrontPage\Admin\BlogController@index')->name('news');
Route::get('/news','FrontPage\Admin\BlogController@index')->name('news');
Route::get('/news/{slug}','FrontPage\Admin\BlogController@show')->name('news.show');

Route::get('berita/{uuid}/{name}','FrontPage\Admin\BlogController@takmir')->name('berita.takmir');
Route::get('/berita/{slug}','FrontPage\Admin\BlogController@takmirshow')->name('berita.show');
Route::get('/jadwal/{slug}','FrontPage\Admin\BlogController@takmirshow')->name('jadwal.show');

// gallery
// Route::get('gallery/{uuid}/{name}','FrontPage\Admin\BlogController@takmir')->name('berita.takmir');
Route::get('/album/{uuid}','FrontPage\Admin\BlogController@gallery')->name('album.show');

// Route::get('web', 'FrontPage\Takmir\WebPageController@index')->name('webfrontpage');
Route::get('web/{uuid}/{name}', 'FrontPage\Takmir\WebPageController@index')->name('web');

Auth::routes();

Route::get('/redirect', 'SocialAuthGoogleController@redirect')->name('redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback')->name('callback');

Auth::routes(['verify' => true]);

Route::group(['middleware'=>['auth','verified']], function () {
    Route::get('test',function(){
        return view('dashboard.takmir.organization.organization');
    });
    Route::get('/home', 'HomeController@index')->name('home');

    // Blog
    Route::resource('blog','Dashboard\BlogController');
    Route::get('api.blog','Dashboard\BlogController@blogApi')->name('api.blog');

    // Jamah
    Route::resource('jamaah', 'Dashboard\Takmir\JamaahController');
    Route::get('api.jamaah', 'Dashboard\Takmir\JamaahController@jamaahApi')->name('api.jamaah');

    // Organization Start
    Route::get('organisasi','Dashboard\Takmir\OrganizationController@index')->name('organisasi');
    Route::get('org/{uuid}','Dashboard\Takmir\OrganizationController@index')->name('org');
    
    Route::get('org/{uuid}/edit','Dashboard\Takmir\OrganizationController@editStructure')->name('org.edit');
    Route::PATCH('org/{uuid}/update','Dashboard\Takmir\OrganizationController@updateStructure')->name('org.update');

    Route::get('org/job/{uuid}/edit','Dashboard\Takmir\OrganizationController@editJobdesc')->name('org.editjob');
    Route::PATCH('org/job/{uuid}/update','Dashboard\Takmir\OrganizationController@updateJobdesc')->name('org.updatejob');
    // Organization End

    // Profile
    Route::get('profile','Dashboard\Takmir\ProfileController@index')->name('profile');
    Route::get('profile/edit/{uuid}','Dashboard\Takmir\ProfileController@edit')->name('profile.edit');
    Route::PATCH('profile/update/{id}','Dashboard\Takmir\ProfileController@update')->name('profile.update');
    Route::get('profile/passEdit/{uuid}','Dashboard\Takmir\ProfileController@passEdit')->name('profile.passEdit');
    Route::PATCH('profile/passUpdate/{uuid}','Dashboard\Takmir\ProfileController@passUpdate')->name('profile.passUpdate');

    // Debit
    Route::resource('debit', "Dashboard\Takmir\Finance\DebetController");
    Route::resource('credit', "Dashboard\Takmir\Finance\CreditController");

    // Gallery
    // Album
    Route::resource('gallery', "Dashboard\Takmir\GallerytionController");
    // Foto
    Route::get('foto/{id}', "Dashboard\Takmir\GallerytionController@createfoto")->name('create.foto');
    Route::get('edit/foto/{id}', "Dashboard\Takmir\GallerytionController@editfoto")->name('edit.foto');
    Route::post('foto/{id}', "Dashboard\Takmir\GallerytionController@storefoto")->name('store.foto');
    Route::put('update/foto/{id}', "Dashboard\Takmir\GallerytionController@updatefoto")->name('update.foto');

    Route::group(['middleware' => ['role:takmir']], function() {
        Route::get('api.debit', "Dashboard\Takmir\Finance\DebetController@apidebit")->name('api.debit');
        Route::get('api.credit', "Dashboard\Takmir\Finance\CreditController@apicredit")->name('api.credit');
        
        // Saran
        Route::get('saran', "Dashboard\Admin\SuggestionController@create")->name('saran.create');
        Route::post('saran', "Dashboard\Admin\SuggestionController@store")->name('saran.store');
        
        // Gallery
        Route::get('api.gallery', "Dashboard\Takmir\GallerytionController@apifoto")->name('api.gallery');

        // help
        Route::get('/help', function () {
            return view('help');
        })->name('help');
    });
    
    
    Route::resource('takmir', 'Dashboard\Admin\User\TakmirController');
    
    Route::group(['middleware' => ['role:admin']], function () {
        
        Route::get('api.takmir', 'Dashboard\Admin\User\TakmirController@takmirApi')->name('api.takmir');
        Route::get('api.saran', "Dashboard\Admin\SuggestionController@saranapi")->name('api.saran');
        
        Route::resource('admin', 'Dashboard\Admin\User\AdminController');
        Route::get('api.admin', 'Dashboard\Admin\User\AdminController@adminApi')->name('api.admin');
        Route::delete('saran/{saran}', "Dashboard\Admin\SuggestionController@destroy")->name('saran.delete');
        Route::get('saran/{saran}', "Dashboard\Admin\SuggestionController@show")->name('saran.show');

    });
});
