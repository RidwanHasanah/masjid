<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class DebetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('debets')->insert(array(
            [
                'takmir_id' => 1,
                'uuid' => Uuid::uuid4(),
                'info' => "kas masjid",
                'nominal' => 10000000,
                'created_at' => "2020-01-19 02:07:26",
                'updated_at' => "2020-01-19 02:07:26"
            ]
            ,
            [
                'takmir_id' => 1,
                'uuid' => Uuid::uuid4(),
                'info' => "kas masjid",
                'nominal' => 2000000,
                'created_at' => now(),
                'updated_at' => now()
            ]
            ,
            [
                'takmir_id' => 1,
                'uuid' => Uuid::uuid4(),
                'info' => "sumbangan",
                'nominal' => 500000,
                'created_at' => "2020-01-23 02:07:26",
                'updated_at' => "2020-01-23 02:07:26"
                
            ]
            ,
            [
                'takmir_id' => 1,
                'uuid' => Uuid::uuid4(),
                'info' => "infaq masjid",
                'nominal' => 100000,
                'created_at' => "2020-01-24 02:07:26",
                'updated_at' => "2020-01-24 02:07:26"
                
            ]            
            ,
            [
                'takmir_id' => 1,
                'uuid' => Uuid::uuid4(),
                'info' => "infaq masjid",
                'nominal' => 400000,
                'created_at' => "2020-01-26 02:07:26",
                'updated_at' => "2020-01-26 02:07:26"
                
            ]
            ,
            [
                'takmir_id' => 1,
                'uuid' => Uuid::uuid4(),
                'info' => "sumbangan",
                'nominal' => 500000,
                'created_at' => "2020-01-27 02:07:26",
                'updated_at' => "2020-01-27 02:07:26"
                
            ]
        ));
    }
}
