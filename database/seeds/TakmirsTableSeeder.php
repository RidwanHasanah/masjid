<?php

use Illuminate\Database\Seeder;

class TakmirsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('takmirs')->insert(array(
            [
                'user_id' => 3,
                'province' => 'Kepulauan Bangka Belitung',
                'gender' => 'Pria',
                'hp' => '08892907236'
            ]
        ));
    }
}
