<?php
require 'vendor/autoload.php';

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Seeder;

class JamaahsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jamaahs')->insert(array(
            [
                'name' => 'Hanif',
                'uuid' => Uuid::uuid4(),
                'takmir_id' => 1,
                'province' => 'Jakarta',
                'gender' => 'Pria',
            ],
            [
                'name' => 'Daud',
                'uuid' => Uuid::uuid4(),
                'takmir_id' => 1,
                'province' => 'Jakarta',
                'gender' => 'Pria',
            ],
            [
                'name' => 'Lukman',
                'uuid' => Uuid::uuid4(),
                'takmir_id' => 1,
                'province' => 'Jakarta',
                'gender' => 'Pria',
            ]

        ));
    }
}
