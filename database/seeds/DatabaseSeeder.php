<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            RolesTableSeeder::class,
            UserRolesTableSeeder::class,
            AdminsTableSeeder::class,
            TakmirsTableSeeder::class,
            MosquesTableSeeder::class,
            JamaahsTableSeeder::class,
            DebetsTableSeeder::class,
            CreditsTableSeeder::class,
            OrganizationsTableSeeder::class,
            JobdescOrgsTableSeeder::class,
        ]);
    }
}
