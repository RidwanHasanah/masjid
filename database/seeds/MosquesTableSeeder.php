<?php

use Illuminate\Database\Seeder;

class MosquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mosques')->insert(array(
            [
                'name' => 'Nurul Falah',
                'username' => 'Nurul Falah Sinar Laut',
                'slug' => 'nurul_falah_sinar_laut',
                'takmir_id' => 1,
                'province' => 'Kepulauan Bangka Belitung',
                'address' => 'jl sinarlaut, Koba - Bangka Tengah',
                'gmaps' => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3288.5367116458037!2d110.3816985142326!3d-7.851065980200792!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a576f3d2c79d9%3A0x719f9eb989f50a05!2sPondok%20IT%20Pusat!5e1!3m2!1sid!2sid!4v1580716590154!5m2!1sid!2sid",
                'desc' => "Di bangun pada tahu 1980",
                'visi' => "Memakmurkan Rumah Allah"
            ]            
        ));
    }
}
