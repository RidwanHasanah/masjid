<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert(array(
            [
                'user_id' => 2,
                'province' => 'Jakarta',
                'gender' => 'Pria',
            ]
        ));
    }
}
