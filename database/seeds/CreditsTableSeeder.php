<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class CreditsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credits')->insert(array(
            [
                'takmir_id' => 1,
                'uuid' => Uuid::uuid4(),
                'info' => "semen",
                'nominal' => 1500000,
                'created_at' => "2020-01-19 02:07:26",
                'updated_at' => "2020-01-19 02:07:26"
            ]
        ));
    }
}
