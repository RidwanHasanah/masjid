<?php
require 'vendor/autoload.php';

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            [
                'id' => 1,
                'uuid' => Uuid::uuid4(),
                'name' => 'Master',
                'email_verified_at' => now(),
                'email' => 'master@pondokit.com',
                'password' => bcrypt('12345678'),
            ],
            [
                'id' => 2,
                'uuid' => Uuid::uuid4(),
                'name' => 'Admin',
                'email' => 'admin@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],
            [
                'id' => 3,
                'uuid' => Uuid::uuid4(),
                'name' => 'Hamsah',
                'email' => 'takmir@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ]
        ));
    }
}
