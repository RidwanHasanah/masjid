<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Ramsey\Uuid\Uuid;
use App\Models\Blog;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'content' => $faker->name.$faker->unique()->safeEmail.$faker->name,
        'created_at' => now(),
        'updated_at' => now(),
        'user_id' => 3,
        'uuid' => Uuid::uuid4(),
        'slug' => str_random(15),
        'cat' => 'takmir'
    ];
});
