<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMosquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('takmir_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('username')->unique()->nullable();
            $table->string('slug')->unique()->nullable();
            $table->string('photo')->nullable();
            $table->text('address')->nullable();
            $table->text('gmaps')->nullable();
            $table->string('province')->nullable();
            $table->text('desc')->nullable();
            $table->text('visi')->nullable();
            $table->text('misi')->nullable();
            $table->timestamps();

            $table->foreign('takmir_id')
            ->references('id')
            ->on('takmirs')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosques');
    }
}
