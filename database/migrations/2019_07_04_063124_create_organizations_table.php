<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('takmir_id')->unsigned();
            $table->text('pelindung')->nullable();
            $table->text('penasehat')->nullable();
            $table->text('ketua')->nullable();
            $table->text('sekretaris')->nullable();
            $table->text('bendahara')->nullable();
            $table->text('humas')->nullable();
            $table->text('keagamaan')->nullable();
            $table->text('kepemudaan')->nullable();
            $table->text('pelaksana_umum')->nullable();
            $table->text('tarbiyah')->nullable();
            $table->text('pengajian')->nullable();
            $table->text('imam')->nullable();
            $table->text('muadzin')->nullable();
            $table->text('remaja')->nullable();
            $table->text('keamanan')->nullable();
            $table->text('marbot')->nullable();
            $table->timestamps();

            $table->foreign('takmir_id')
           ->references('id')
           ->on('takmirs')
           ->onUpdate('cascade')
           ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
